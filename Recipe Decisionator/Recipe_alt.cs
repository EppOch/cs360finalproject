﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Recipe_Decisionator
{
    public class Recipe
    {
        public int key { get; set; }
        public string Name { get; set; }
        public bool Favorite { get; set; }
        public uint PrepTime { get; set; }
        public string Description { get; set; }
        public string Directions { get; set; }
        public List<QuantifiedIngredient> ingredients { get; set; }

        public override string ToString()
        {
            return Name + '\n' + 
                (Favorite ? "Favorite Recipe!\n" : "") + 
                "Preparation Time: " + PrepTime.ToString() + " minutes\n" + 
                Description + '\n';
        }

        public DataTable getIngredientsTable()
        {
            // create a new table to hold the ingredients
            DataTable contents = new DataTable("Ingredients");
            // format the table columns to prepare them for use
            contents.Columns.Add("Name", typeof(string));
            contents.Columns.Add("Unit", typeof(string));
            contents.Columns.Add("Quantity", typeof(double?));
            // since none of this will be linked to anything just need to fill the table
            DataRow ing;
            foreach (QuantifiedIngredient QI in ingredients)
            {
                ing = contents.NewRow();
                ing.SetField<string>("Name", QI.Name);
                ing.SetField<string>("Unit", QI.unit.Label);
                ing.SetField<double?>("Quantity", QI.quantity);
                contents.ImportRow(ing);
            }
            return contents;
        }
        
        public void fromIngredientsTable() { }
    }
}