﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Recipe_Decisionator
{
    partial class RecipesDataSet
    {
        partial class UnitsDataTable
        {
            /// <summary>
            /// Method that returns a keyed collection of all the units present in the data-table
            /// </summary>
            /// <returns></returns>
            public Dictionary<string, Unit> getUnits()
            {
                Dictionary<string, Unit> TempUnits = new Dictionary<string, Unit>();
                // Pre-build the dictionary of units using only the labels and the factors
                foreach (DataRow DRU in this)
                {
                    TempUnits.Add(DRU.Field<string>("Label"), new Unit(DRU.Field<string>("Label"), DRU.Field<double>("Factor")));
                }
                foreach (DataRow DRU in this)
                {
                    if (DRU.Field<string>("Inheritance") != null)
                    {
                        if (TempUnits.ContainsKey(DRU.Field<string>("Inheritance")))
                        {
                            TempUnits[DRU.Field<string>("Label")].Inheritance = TempUnits[DRU.Field<string>("Inheritance")];
                        }
                    }
                }
                return TempUnits;
            }
        }

        partial class FridgeDataTable
        {
            /// <summary>
            /// Retrieves all ingredients from the fridge and puts them in an indexible format
            /// </summary>
            /// <returns></returns>
            public Dictionary<string, QuantifiedIngredient> getIngredient(Dictionary<string, Unit> Units)
            {
                Dictionary<string, QuantifiedIngredient> TempIngredient = new Dictionary<string, QuantifiedIngredient>();
                foreach (DataRow DR in this)
                {
                    TempIngredient.Add(DR.Field<string>("Name"), new QuantifiedIngredient(DR.Field<string>("Name"), Units[DR.Field<string>("Unit")], DR.Field<double>("Quantity")));
                }
                return TempIngredient;
            }

            /// <summary>
            /// Returns all ingredients in fridge that match the given list of ingredients (fuzzy)
            /// </summary>
            /// <param name="ingredients"></param>
            /// <param name="Units"></param>
            /// <returns></returns>
            public Dictionary<string, QuantifiedIngredient> searchIngredient(string ingredients, Dictionary<string, Unit> Units)
            {
                Dictionary<string, QuantifiedIngredient> TempIngredient = new Dictionary<string, QuantifiedIngredient>();
                foreach (DataRow DR in this.Select(string.Format("Name LIKE '*{0}*'", ingredients)))
                {
                    TempIngredient.Add(DR.Field<string>("Name"), new QuantifiedIngredient(DR.Field<string>("Name"), Units[DR.Field<string>("Unit")], DR.Field<double>("Quantity")));
                }
                return TempIngredient;
            }
        }

        partial class ShoppingListDataTable
        {
            /// <summary>
            /// Method to extract the ingredients from the shopping list
            /// </summary>
            /// <returns></returns>
            public List<QuantifiedIngredient> getIngredient() { return null; }
        }

        partial class RecipeIngredientsDataTable
        {
            /// <summary>
            /// Method that transforms a table of ingredients into a list of ingredients
            /// </summary>
            /// <returns></returns>
            public List<QuantifiedIngredient> getIngredient() { return null; }

            /// <summary>
            /// Method that adds the ingredients of the recipe to the master table
            /// </summary>
            /// <returns></returns>
            public void addRecipeIngredients(Recipe newRecipe)
            {
                foreach (var Ing in newRecipe.ingredients)
                {
                    RecipeIngredientsRow nRI = this.NewRecipeIngredientsRow();
                    nRI.Recipe = newRecipe.key;
                    nRI.Name = Ing.Name;
                    nRI.Unit = Ing.unit.Label;
                    nRI.Quantity = Ing.quantity;
                    this.AddRecipeIngredientsRow(nRI);
                }
            }


            /// <summary>
            /// Method that finds Recipes that use only the listed ingredients
            /// </summary>
            /// <param name="ingredients"></param>
            /// <returns></returns>
            public List<Recipe> SearchByIngredients(List<string> ingredients) { return null; }
        }

        partial class RecipeDataTable
        {
            /// <summary>
            /// Method for returning all recipes as a list of recipes
            /// </summary>
            /// <returns></returns>
            public List<Recipe> getRecipes() { return null; }

            /// <summary>
            /// Method for adding a recipe to the database
            /// </summary>
            /// <returns></returns>
            public void addRecipe(Recipe newRecipe)
            {
                //Add recipe parameters to table
                RecipeRow nR = this.NewRecipeRow();
                nR.Name = newRecipe.Name;
                nR.Favorite = newRecipe.Favorite;
                nR.PrepTime = newRecipe.PrepTime;
                nR.Description = newRecipe.Description;
                nR.Directions = newRecipe.Directions;
                this.AddRecipeRow(nR);
            }


            /// <summary>
            /// Method for searching the database by name of recipe
            /// </summary>
            /// <param name="Name"></param>
            /// <returns></returns>
            public List<Recipe> getRecipes(string Name, Dictionary<string, Unit> units)
            {
                Recipe TempRecipe;
                QuantifiedIngredient TempIngredient = new QuantifiedIngredient();

                List<Recipe> R = new List<Recipe>();
                foreach (DataRow D in this.Select(string.Format("Name LIKE '{0}'", "*" + Name + "*")))
                {
                    // makes an empty recipe
                    TempRecipe = new Recipe();
                    // fills in recipe details
                    TempRecipe.Name = D.Field<string>("Name");
                    TempRecipe.Favorite = D.Field<bool>("Favorite");
                    TempRecipe.Directions = D.Field<string>("Directions");
                    TempRecipe.Description = D.Field<string>("Description");
                    TempRecipe.PrepTime = D.Field<uint>("PrepTime");
                    // fills in the recipe ingredients
                    foreach (DataRow ID in D.GetChildRows("Recipe_RecipeIngredients"))
                    {
                        TempRecipe.ingredients.Add(new QuantifiedIngredient(ID.Field<string>("Name"), units[ID.Field<string>("Unit")], ID.Field<double>("Quantity")));
                    }
                    R.Add(TempRecipe);
                }
                return R;
            }

            /// <summary>
            /// Method for retrieving all recipes that can be made with only the ingredients
            /// in a list
            /// </summary>
            /// <param name="ingredients"></param>
            /// <returns></returns>
            public List<Recipe> SearchByIngredients(List<ingredient> ingredients)
            {
                return null;






            }
        }
    }
}
