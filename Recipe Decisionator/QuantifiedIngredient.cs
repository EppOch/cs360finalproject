﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipe_Decisionator
{
    public class QuantifiedIngredient : ingredient
    {
        public Unit unit;
        public double quantity;

        public QuantifiedIngredient() { }
        public QuantifiedIngredient(string name, Unit unit, double amount)
        {
            this.Name = name;
            this.unit = unit;
            this.quantity = amount;
        }
    }
}
