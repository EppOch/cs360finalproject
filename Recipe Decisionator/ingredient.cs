﻿namespace Recipe_Decisionator
{
    public class ingredient
    {
        /// <summary>
        /// The name of the ingredient
        /// </summary>
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}