﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Recipe_Decisionator
{
    //Recipe entry class
    //Each recipe has a key which is null until added to a recipe list
    //and a title, rating, directions, a description, time to cook, and a list of ingredients
    public class recipe
    {

        //Public variables
        public int? key { get; set; }
        public string title { get; set; }
        public bool rating { get; set; }
        public string dir { get; set; }
        public string desc { get; set; }
        public int time { get; set; }
        public ingQTable ing;


        //Constructors

        //Empty
        public recipe()
        {
            key = null;
            title = "food";
            rating = false;
            dir = "cook";
            desc = "good";
            time = 0;
            ing = new ingQTable(title);
        }


        //Standard
        public recipe(string t, bool r, string di, string de, int ti, DataTable i)
        {
            title = t;
            rating = r;
            dir = di;
            desc = de;
            time = ti;
            ing = new ingQTable(i, title);
        }


        //Public functions
        
        //Updates the ingredient table in database after it has been retrieved with getIng()
        public bool modifyIng(DataTable i)
        {
            bool success = ing.modifyIngList(i);

           if (success) { return true; } else { return false; }
        }

        //Retrieves ingredient table from database
        public DataTable getIng()
        {
            return ing.getIng();
        }
    }
}
