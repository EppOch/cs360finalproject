﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recipe_Decisionator
{
    enum unit{Weight,Volume,RawQuantity}
    //Default Ingredient SQLite Table Class
    //Columns: id = primary key, name = string
    public class ingTable
    {

        //public variables
        public SQLiteTable table;
        public string tableName { get; set; }
        public string dBName = "C:\\decisionator.sqlite";
        public bool isPresent {get; set; }

        //Constructors

        //Do not use, tables can not be stored without a name
        public ingTable() { }
        //Creates an ingredient table in database with the given name
        public ingTable(string name)
        {
            tableName = name;
            table = new SQLiteTable(tableName);
            using (SQLiteConnection conn = new SQLiteConnection("data source=" + dBName))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;
                    conn.Open();

                    SQLiteHelper sh = new SQLiteHelper(cmd);
                    table = new SQLiteTable(tableName);

                    table.Columns.Add(new SQLiteColumn("id", true));
                    table.Columns.Add(new SQLiteColumn("name", ColType.Text));
                    sh.CreateTable(table);

                    conn.Close();
                }
            }
        }

        //Creates an ingredient table in database with the given name and populates it with data in ingL
        public ingTable(DataTable ingL, string name)
        {
            tableName = name;
            table = new SQLiteTable(tableName);
            using (SQLiteConnection conn = new SQLiteConnection("data source=C:\\decisionator.sqlite"))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;
                    conn.Open();

                    SQLiteHelper sh = new SQLiteHelper(cmd);
                    SQLiteTable table = new SQLiteTable(tableName);

                    table.Columns.Add(new SQLiteColumn("id", true));
                    table.Columns.Add(new SQLiteColumn("name", ColType.Text));
                    sh.CreateTable(table);

                    
                    SQLiteDataAdapter sda = new SQLiteDataAdapter();
                    sda.SelectCommand = new SQLiteCommand("SELECT * from " + tableName, conn);
                    SQLiteCommandBuilder builder = new SQLiteCommandBuilder(sda);
                    builder.GetInsertCommand();
                    sda.Update(ingL);

                    conn.Close();
                }
            }
        }


        //public functions

        //Updates table with changes made.  INPUT TABLE MUST BE RETRIEVED USING getIng() BEFORE CHANGES ARE MADE
        public bool modifyIngList(DataTable ingL)
        {
            using (SQLiteConnection conn = new SQLiteConnection("data source=C:\\decisionator.sqlite"))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;
                    conn.Open();

                    try
                    {
                        SQLiteDataAdapter sda = new SQLiteDataAdapter();
                        sda.SelectCommand = new SQLiteCommand("SELECT * from " + tableName, conn);
                        SQLiteCommandBuilder builder = new SQLiteCommandBuilder(sda);
                        builder.GetInsertCommand();
                        builder.GetDeleteCommand();
                        builder.GetUpdateCommand();
                        sda.Update(ingL);
                        conn.Close();
                        return true;
                    }
                    catch
                    {
                        conn.Close();
                        return false;
                    }
                }
            }
        }

        //Retrieves the table from the database
        public DataTable getIng()
        {
            using (SQLiteConnection conn = new SQLiteConnection("data source=C:\\decisionator.sqlite"))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;
                    conn.Open();

                    DataTable ingT = new DataTable();

                    try
                    {
                        SQLiteDataAdapter sda = new SQLiteDataAdapter();
                        sda.SelectCommand = new SQLiteCommand("SELECT * from " + tableName, conn);
                        sda.Fill(ingT);
                        conn.Close();
                        return ingT;
                    }
                    catch
                    {
                        conn.Close();
                        return null;
                    }
                }
            }
        }

        //Saves the table to file
        public bool savelist(string filename, DataTable ingL) { return true; }
    }

    //Quantified Ingredient SQLite Table Class
    //Columns: id = primary key, name = string, quantity = float, unit = enum(m, v, d)
    public class ingQTable : ingTable
    {
        //Constructors

        //Do not use, tables can not be stored without a name
        public ingQTable() { }

        //Creates an ingredient table in database with the given name
        public ingQTable(string name)
        {
            isPresent = false;
            tableName = name;
            table = new SQLiteTable(tableName);
            
            using (SQLiteConnection conn = new SQLiteConnection("data source=C:\\decisionator.sqlite"))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;
                    conn.Open();

                    try
                    {
                        cmd.CommandText = "CREATE TABLE " + tableName + " (id INTEGER PRIMARY KEY, name TEXT, quantity REAL, unit TEXT CHECK(unit IN('Weight', 'Volume', 'RawQuantity')) NOT NULL DEFAULT 'm')";
                        cmd.ExecuteNonQuery();
                    }
                    catch
                    {
                        MessageBox.Show("The recipe already exists.  Please choose a different name.  If you wish to modify the recipe, please navigate to the modify recipe screen.");
                        isPresent = true;
                    }
                    conn.Close();
                }
            }
        }

        //Creates an ingredient table in database with the given name and populates it with data in ingL
        public ingQTable(DataTable ingQL, string name)
        {
            isPresent = false;
            tableName = name;
            table = new SQLiteTable(tableName);
            using (SQLiteConnection conn = new SQLiteConnection("data source=C:\\decisionator.sqlite"))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;
                    conn.Open();

                    try
                    {
                        cmd.CommandText = "CREATE TABLE " + tableName + " (id INTEGER PRIMARY KEY, name TEXT, quantity REAL, unit TEXT CHECK(unit IN('Weight', 'Volume', 'RawQuantity')) NOT NULL DEFAULT 'm')";
                        cmd.ExecuteNonQuery();

                        SQLiteDataAdapter sda = new SQLiteDataAdapter();
                        sda.SelectCommand = new SQLiteCommand("SELECT * from " + tableName, conn);
                        SQLiteCommandBuilder builder = new SQLiteCommandBuilder(sda);
                        builder.GetInsertCommand();
                        sda.Update(ingQL);
                    }
                    catch
                    {
                        MessageBox.Show("The recipe already exists.  Please choose a different name.  If you wish to modify the recipe, please navigate to the modify recipe screen.");
                        isPresent = true;
                    }

                    conn.Close();
                }
            }
        }
    }
}
