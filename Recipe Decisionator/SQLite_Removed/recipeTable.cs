﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recipe_Decisionator
{
    //Recipe List SQLite Table class
    //Columns: id = primary key, title = string, desc = string, dir = string, time = integer, rating = bool
    public class recipeTable
    {
        public SQLiteTable rec;
        public string tableName { get; set; }
        public string dBName = "C:\\decisionator.sqlite";

        //Constructors
        public recipeTable(string name)
        {
            tableName = name;
            rec = new SQLiteTable(tableName);
            rec.Columns.Add(new SQLiteColumn("id", true));
            rec.Columns.Add(new SQLiteColumn("title", ColType.Text));
            rec.Columns.Add(new SQLiteColumn("desc", ColType.Text));
            rec.Columns.Add(new SQLiteColumn("dir", ColType.Text));
            rec.Columns.Add(new SQLiteColumn("time", ColType.Integer));
            rec.Columns.Add(new SQLiteColumn("rating", ColType.Integer));

            using (SQLiteConnection conn = new SQLiteConnection("data source=" + dBName))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.CommandText = "CREATE TABLE IF NOT EXISTS " + tableName + " (ID INTEGER PRIMARY KEY, title TEXT, desc TEXT, dir TEXT, time INTEGER, rating INTEGER)";
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }

        public recipeTable(DataTable recT, string tableName)
        {
            rec = new SQLiteTable(tableName);
            rec.Columns.Add(new SQLiteColumn("id", true));
            rec.Columns.Add(new SQLiteColumn("title", ColType.Text));
            rec.Columns.Add(new SQLiteColumn("desc", ColType.Text));
            rec.Columns.Add(new SQLiteColumn("dir", ColType.Text));
            rec.Columns.Add(new SQLiteColumn("time", ColType.Integer));
            rec.Columns.Add(new SQLiteColumn("rating", ColType.Integer));

            using (SQLiteConnection conn = new SQLiteConnection("data source=" + dBName))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.CommandText = "CREATE TABLE IF NOT EXISTS " + tableName + " (ID INTEGER PRIMARY KEY, title TEXT, desc TEXT, dir TEXT, time INTEGER, rating INTEGER)";
                    cmd.ExecuteNonQuery();

                    SQLiteDataAdapter sda = new SQLiteDataAdapter();
                    sda.SelectCommand = new SQLiteCommand("SELECT * from " + tableName, conn);
                    SQLiteCommandBuilder builder = new SQLiteCommandBuilder(sda);
                    builder.GetInsertCommand();
                    sda.Update(recT);
                    conn.Close();
                }
            }
        }

        //Public Functions

        public int? addRecipe(recipe r)
        {
            using (SQLiteConnection conn = new SQLiteConnection("data source=" + dBName))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;
                    conn.Open();

                    SQLiteHelper sh = new SQLiteHelper(cmd);

                    var row = new Dictionary<string, object>();
                    row["title"] = r.title;
                    row["desc"] = r.desc;
                    row["dir"] = r.dir;
                    row["time"] = r.time;
                    row["rating"] = r.rating;
                    
                    try {
                        sh.Insert(tableName, row);
                        int id = (int)sh.LastInsertRowId();
                        conn.Close();
                        r.key = id;
                        return id;
                    }
                    catch
                    {
                        return null;
                    }


                }
            }
        }

        public DataTable getRecipes()
        {
            using (SQLiteConnection conn = new SQLiteConnection("data source=" + dBName))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;
                    conn.Open();

                    DataTable recT = new DataTable();

                    try
                    {
                        SQLiteDataAdapter sda = new SQLiteDataAdapter();
                        sda.SelectCommand = new SQLiteCommand("SELECT * from " + tableName, conn);
                        sda.Fill(recT);
                        conn.Close();
                        return recT;
                    }
                    catch
                    {
                        conn.Close();
                        return null;
                    }
                }
            }
        }

        public DataTable searchRecipes(string Title)
        {
            using (SQLiteConnection conn = new SQLiteConnection("data source=" + dBName))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;
                    conn.Open();

                    SQLiteHelper sh = new SQLiteHelper(cmd);

                    try {
                        DataTable results = sh.Select("select * from " + tableName + " WHERE UPPER(title) LIKE UPPER('" + Title + "%')");
                        conn.Close();
                        return results;
                    }
                    catch
                    {
                        conn.Close();
                        return null;
                    }

                }
            }
        }

        public bool delRecipe(int? key)
        {
            using (SQLiteConnection conn = new SQLiteConnection("data source=" + dBName))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;
                    conn.Open();

                    try
                    {
                        cmd.CommandText = String.Format("DELETE FROM {0} WHERE id={1}", tableName,key);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                        return true;
                    }
                    catch
                    {
                        conn.Close();
                        return false;
                    }

                }
            }
        }

        public bool saveRecipes(string filename, DataTable recipes)
        {
            return true;
        }

    }
}
