﻿namespace Recipe_Decisionator
{
    public class Unit
    {
        // Data
        public string Label { get; set; }
        public Unit Inheritance { get; set; }
        public double? Factor { get; set; }

        public Unit()
        {
            Label = "";
            Inheritance = null;
            Factor = null;
        }
        public Unit(string lbl)
        {
            Label = lbl;
            Inheritance = null;
            Factor = null;
        }
        public Unit(string lbl, double? scale)
        {
            Label = lbl;
            Inheritance = null;
            Factor = scale;
        }
        public Unit(string lbl, Unit UnitBase, double? scale)
        {
            Label = lbl;
            Inheritance = UnitBase;
            Factor = scale;
        }

        /// <summary>
        /// compares the core unit of the given unit to see if they are convertible
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        public bool Comparable(Unit b)
        {
            return b.coreUnit().Equals(coreUnit());
        }

        /// <summary>
        /// Gets the base unit label of derived units
        /// </summary>
        /// <returns></returns>
        private string coreUnit()
        {
            if (Inheritance != null)
                return Inheritance.coreUnit();
            return Label;
        }

        /// <summary>
        /// compare method returns whether b is bigger or equal to a
        /// </summary>
        /// <param name="quantitya"></param>
        /// <param name="b"></param>
        /// <param name="quantityb"></param>
        /// <returns></returns>
        public bool Compare(double? quantitya, Unit b, double? quantityb)
        {
            // if either is null then always true
            if ((b.Amount(quantityb) == null) || (Amount(quantitya) == null))
                return true;
            if (b.Amount(quantityb) >= Amount(quantitya))
                return true;
            return false;
        }

        /// <summary>
        /// handler method for basetype conversions
        /// </summary>
        /// <param name="quantity"></param>
        /// <returns></returns>
        public double? Amount(double? quantity)
        {
            if ((quantity == null) || (Factor == null))
                return null;
            if (Factor != null)
                return Inheritance.Amount(Factor * quantity);
            return Factor * quantity;
        }
    }
}