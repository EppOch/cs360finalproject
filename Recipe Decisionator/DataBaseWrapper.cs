﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;

namespace Recipe_Decisionator
{
    class DataBaseWrapper
    {
        /// <summary>
        /// Default constructor, sets up the table nicely
        /// </summary>
        public DataBaseWrapper()
        {
            // use the default save location
            DBFileName = "./DatabaseFile.xml";

            AddUnitTypes();
            clearDatabase(); // May remove or make this line conditional if problems occur.
            initialize(DefaultUnits.FindAll(u => u.Inheritance == null), DefaultUnits.FindAll(u => u.Inheritance != null));
        }

        // Defaults
        public static string DBFileName { get; private set; }
        public static List<Unit> DefaultUnits;
        private static RecipesDataSet RecipeDatabase;

        // Accessible Methods
        public List<Recipe> FindRecipe(List<string> searchTerms) { return null; }
        public List<Recipe> getRecipeFromIngredients(List<ingredient> ingredients) { return null; }

        // hidden methods
        private DataRow[] getRecipeFromIngredients(DataRow[] ingredients) { return null; }
        private RecipesDataSet.UnitsDataTable TableOfUnits(List<Unit> basicUnits) { return null; }
        private void AddUnitTypes()
        {
            // Add Base Units to the DefaultUnits
            DefaultUnits = new List<Unit>();
            DefaultUnits.Add(new Unit("NONFINITE", null));
            DefaultUnits.Add(new Unit("PIECES", 1));
            DefaultUnits.Add(new Unit("VOLUME", 1));
            DefaultUnits.Add(new Unit("MASS", 1));
            // Add Derived Units here
            DefaultUnits.Add(new Unit("Loaf", DefaultUnits.Find(u => u.Label.Contains("PIECES")), 20));
            DefaultUnits.Add(new Unit("Slice", DefaultUnits.Find(u => u.Label.Contains("Loaf")), .05));
        }

        
        // Methods that initialize things

        /// <summary>
        /// Deletes the database file
        /// </summary>
        public void clearDatabase()
        {
            // cleans up old database efficiently, remove this when running for real.
            if (File.Exists(DBFileName)) { File.Delete(DBFileName); }
        }

        /// <summary>
        /// method for creating the database file and populating it
        /// </summary>
        /// <param name="baseUnit"></param>
        /// <param name="DerivedUnit"></param>
        public void initialize(List<Unit> baseUnit, List<Unit> DerivedUnit)
        {
            // Initialize a global Dataset File to start things off
            RecipeDatabase = new RecipesDataSet();

            if (!File.Exists(DBFileName))
            {
                RecipesDataSet.RecipeRow fragment;
                RecipesDataSet.UnitsRow nonfinite, chunk, cup, weight;
                // Add the Default units to the table
                // Add the simplest unit types
                nonfinite = RecipeDatabase.Units.AddUnitsRow("NONFINITE", 1, null);
                chunk = RecipeDatabase.Units.AddUnitsRow("PIECES", 1, null);
                cup = RecipeDatabase.Units.AddUnitsRow("VOLUME", 1, null);
                weight = RecipeDatabase.Units.AddUnitsRow("MASS", 1, null);

                // Add A recipe for toast as a base case
                fragment = RecipeDatabase.Recipe.AddRecipeRow("Toast", true, 2, "Baked, Sliced Bread", "Put one slice in toaster and bake.");
                RecipeDatabase.RecipeIngredients.AddRecipeIngredientsRow(fragment, "Slice of bread", chunk, 1);

                // Put some bread in the pantry so we can make some toast
                RecipeDatabase.Fridge.AddFridgeRow("Slice of bread", chunk, 1);

                // Clean out the shopping list like I bought something
                RecipeDatabase.ShoppingList.Clear();

                // create the database file and clean up objects so they can be read
                RecipeDatabase.WriteXml(DBFileName);
                RecipeDatabase.Clear();
            }

            if (File.Exists(DBFileName)) { RecipeDatabase.ReadXml(DBFileName, XmlReadMode.IgnoreSchema); }

            /* // This part is not stable and should be an example of how to access the database
            DataRow[] foundRows;
            // This part looks for ingredients ending with bread
            foundRows = RecipeDatabase.RecipeIngredients.Select(string.Format("Name LIKE '{0}'", "*bread*"));
            // This part finds the first recipe containing bread
            Console.WriteLine(RecipeDatabase.RecipeIngredients.Select(string.Format("Name LIKE '{0}'", "*bread"))[0].GetParentRow("Recipe_RecipeIngredients").Field<string>("Description").ToString());
            foreach (DataRow dr in foundRows)
            {
                Console.WriteLine(dr.GetParentRow("Recipe_RecipeIngredients").Field<string>("Name").ToString());
            }

            Console.WriteLine(foundRows.ToList().Count.ToString());
            // Wait for a keystroke to end the program
            Console.ReadKey();
            */
        }
    }
}
