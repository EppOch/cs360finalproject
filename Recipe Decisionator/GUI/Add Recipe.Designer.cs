﻿namespace Recipe_Decisionator
{
    partial class Add_Recipe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Save_PB = new System.Windows.Forms.Button();
            this.Unit_Type = new System.Windows.Forms.Label();
            this.Quanitity_Label = new System.Windows.Forms.Label();
            this.Name_Label = new System.Windows.Forms.Label();
            this.Cancel_PB = new System.Windows.Forms.Button();
            this.Remove_PB = new System.Windows.Forms.Button();
            this.Add_PB = new System.Windows.Forms.Button();
            this.Unit_Type_CB = new System.Windows.Forms.ComboBox();
            this.Quantity_Numeric = new System.Windows.Forms.NumericUpDown();
            this.Name_TB = new System.Windows.Forms.TextBox();
            this.Ingredients_Label = new System.Windows.Forms.Label();
            this.Directions_Label = new System.Windows.Forms.Label();
            this.Title_Label = new System.Windows.Forms.Label();
            this.Description_Label = new System.Windows.Forms.Label();
            this.Ingredients_Table = new System.Windows.Forms.DataGridView();
            this.Recipe_Directions_TB = new System.Windows.Forms.TextBox();
            this.Recipe_Description_TB = new System.Windows.Forms.TextBox();
            this.Recipe_Title_TB = new System.Windows.Forms.TextBox();
            this.Note_Label = new System.Windows.Forms.Label();
            this.Title = new System.Windows.Forms.Label();
            this.Favorite = new System.Windows.Forms.RadioButton();
            this.Time = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.Unit_CB = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.Quantity_Numeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ingredients_Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Time)).BeginInit();
            this.SuspendLayout();
            // 
            // Save_PB
            // 
            this.Save_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Save_PB.Location = new System.Drawing.Point(496, 874);
            this.Save_PB.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Save_PB.Name = "Save_PB";
            this.Save_PB.Size = new System.Drawing.Size(128, 54);
            this.Save_PB.TabIndex = 51;
            this.Save_PB.Text = "Save";
            this.Save_PB.UseVisualStyleBackColor = true;
            this.Save_PB.Click += new System.EventHandler(this.Save_PB_Click);
            // 
            // Unit_Type
            // 
            this.Unit_Type.AutoSize = true;
            this.Unit_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Unit_Type.Location = new System.Drawing.Point(555, 688);
            this.Unit_Type.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Unit_Type.Name = "Unit_Type";
            this.Unit_Type.Size = new System.Drawing.Size(116, 29);
            this.Unit_Type.TabIndex = 50;
            this.Unit_Type.Text = "Unit Type";
            // 
            // Quanitity_Label
            // 
            this.Quanitity_Label.AutoSize = true;
            this.Quanitity_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Quanitity_Label.Location = new System.Drawing.Point(381, 688);
            this.Quanitity_Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Quanitity_Label.Name = "Quanitity_Label";
            this.Quanitity_Label.Size = new System.Drawing.Size(106, 29);
            this.Quanitity_Label.TabIndex = 49;
            this.Quanitity_Label.Text = "Quantitiy";
            // 
            // Name_Label
            // 
            this.Name_Label.AutoSize = true;
            this.Name_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name_Label.Location = new System.Drawing.Point(10, 683);
            this.Name_Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Name_Label.Name = "Name_Label";
            this.Name_Label.Size = new System.Drawing.Size(78, 29);
            this.Name_Label.TabIndex = 48;
            this.Name_Label.Text = "Name";
            // 
            // Cancel_PB
            // 
            this.Cancel_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancel_PB.Location = new System.Drawing.Point(712, 874);
            this.Cancel_PB.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Cancel_PB.Name = "Cancel_PB";
            this.Cancel_PB.Size = new System.Drawing.Size(128, 54);
            this.Cancel_PB.TabIndex = 47;
            this.Cancel_PB.Text = "Cancel";
            this.Cancel_PB.UseVisualStyleBackColor = true;
            this.Cancel_PB.Click += new System.EventHandler(this.Cancel_PB_Click);
            // 
            // Remove_PB
            // 
            this.Remove_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Remove_PB.Location = new System.Drawing.Point(496, 766);
            this.Remove_PB.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Remove_PB.Name = "Remove_PB";
            this.Remove_PB.Size = new System.Drawing.Size(128, 54);
            this.Remove_PB.TabIndex = 46;
            this.Remove_PB.Text = "Remove";
            this.Remove_PB.UseVisualStyleBackColor = true;
            this.Remove_PB.Click += new System.EventHandler(this.Remove_PB_Click);
            // 
            // Add_PB
            // 
            this.Add_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Add_PB.Location = new System.Drawing.Point(298, 766);
            this.Add_PB.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Add_PB.Name = "Add_PB";
            this.Add_PB.Size = new System.Drawing.Size(128, 54);
            this.Add_PB.TabIndex = 45;
            this.Add_PB.Text = "Add";
            this.Add_PB.UseVisualStyleBackColor = true;
            this.Add_PB.Click += new System.EventHandler(this.Add_PB_Click);
            // 
            // Unit_Type_CB
            // 
            this.Unit_Type_CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Unit_Type_CB.FormattingEnabled = true;
            this.Unit_Type_CB.Location = new System.Drawing.Point(514, 717);
            this.Unit_Type_CB.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Unit_Type_CB.Name = "Unit_Type_CB";
            this.Unit_Type_CB.Size = new System.Drawing.Size(184, 37);
            this.Unit_Type_CB.TabIndex = 44;
            // 
            // Quantity_Numeric
            // 
            this.Quantity_Numeric.AllowDrop = true;
            this.Quantity_Numeric.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Quantity_Numeric.Location = new System.Drawing.Point(386, 717);
            this.Quantity_Numeric.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Quantity_Numeric.Name = "Quantity_Numeric";
            this.Quantity_Numeric.Size = new System.Drawing.Size(82, 33);
            this.Quantity_Numeric.TabIndex = 43;
            // 
            // Name_TB
            // 
            this.Name_TB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name_TB.Location = new System.Drawing.Point(15, 715);
            this.Name_TB.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name_TB.Name = "Name_TB";
            this.Name_TB.Size = new System.Drawing.Size(310, 33);
            this.Name_TB.TabIndex = 42;
            // 
            // Ingredients_Label
            // 
            this.Ingredients_Label.AutoSize = true;
            this.Ingredients_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ingredients_Label.Location = new System.Drawing.Point(21, 502);
            this.Ingredients_Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Ingredients_Label.Name = "Ingredients_Label";
            this.Ingredients_Label.Size = new System.Drawing.Size(139, 29);
            this.Ingredients_Label.TabIndex = 41;
            this.Ingredients_Label.Text = "Ingredients:";
            // 
            // Directions_Label
            // 
            this.Directions_Label.AutoSize = true;
            this.Directions_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Directions_Label.Location = new System.Drawing.Point(27, 392);
            this.Directions_Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Directions_Label.Name = "Directions_Label";
            this.Directions_Label.Size = new System.Drawing.Size(127, 29);
            this.Directions_Label.TabIndex = 40;
            this.Directions_Label.Text = "Directions:";
            // 
            // Title_Label
            // 
            this.Title_Label.AutoSize = true;
            this.Title_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title_Label.Location = new System.Drawing.Point(87, 191);
            this.Title_Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Title_Label.Name = "Title_Label";
            this.Title_Label.Size = new System.Drawing.Size(67, 29);
            this.Title_Label.TabIndex = 38;
            this.Title_Label.Text = "Title:";
            // 
            // Description_Label
            // 
            this.Description_Label.AutoSize = true;
            this.Description_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Description_Label.Location = new System.Drawing.Point(24, 275);
            this.Description_Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Description_Label.Name = "Description_Label";
            this.Description_Label.Size = new System.Drawing.Size(141, 29);
            this.Description_Label.TabIndex = 39;
            this.Description_Label.Text = "Description:";
            // 
            // Ingredients_Table
            // 
            this.Ingredients_Table.AllowUserToDeleteRows = false;
            this.Ingredients_Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Ingredients_Table.Location = new System.Drawing.Point(154, 502);
            this.Ingredients_Table.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Ingredients_Table.Name = "Ingredients_Table";
            this.Ingredients_Table.ReadOnly = true;
            this.Ingredients_Table.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Ingredients_Table.Size = new System.Drawing.Size(519, 148);
            this.Ingredients_Table.TabIndex = 37;
            // 
            // Recipe_Directions_TB
            // 
            this.Recipe_Directions_TB.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Recipe_Directions_TB.Location = new System.Drawing.Point(154, 382);
            this.Recipe_Directions_TB.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Recipe_Directions_TB.Multiline = true;
            this.Recipe_Directions_TB.Name = "Recipe_Directions_TB";
            this.Recipe_Directions_TB.Size = new System.Drawing.Size(692, 84);
            this.Recipe_Directions_TB.TabIndex = 36;
            // 
            // Recipe_Description_TB
            // 
            this.Recipe_Description_TB.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Recipe_Description_TB.Location = new System.Drawing.Point(154, 265);
            this.Recipe_Description_TB.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Recipe_Description_TB.Multiline = true;
            this.Recipe_Description_TB.Name = "Recipe_Description_TB";
            this.Recipe_Description_TB.Size = new System.Drawing.Size(692, 84);
            this.Recipe_Description_TB.TabIndex = 35;
            // 
            // Recipe_Title_TB
            // 
            this.Recipe_Title_TB.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Recipe_Title_TB.Location = new System.Drawing.Point(154, 180);
            this.Recipe_Title_TB.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Recipe_Title_TB.Name = "Recipe_Title_TB";
            this.Recipe_Title_TB.Size = new System.Drawing.Size(692, 40);
            this.Recipe_Title_TB.TabIndex = 34;
            // 
            // Note_Label
            // 
            this.Note_Label.AutoSize = true;
            this.Note_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Note_Label.Location = new System.Drawing.Point(58, 123);
            this.Note_Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Note_Label.Name = "Note_Label";
            this.Note_Label.Size = new System.Drawing.Size(797, 29);
            this.Note_Label.TabIndex = 33;
            this.Note_Label.Text = "Note: Please fill in as much information as possible for the desired recipe.";
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.Location = new System.Drawing.Point(258, 34);
            this.Title.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(326, 64);
            this.Title.TabIndex = 32;
            this.Title.Text = "Add Recipe";
            // 
            // Favorite
            // 
            this.Favorite.AutoSize = true;
            this.Favorite.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Favorite.Location = new System.Drawing.Point(712, 505);
            this.Favorite.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Favorite.Name = "Favorite";
            this.Favorite.Size = new System.Drawing.Size(137, 33);
            this.Favorite.TabIndex = 52;
            this.Favorite.TabStop = true;
            this.Favorite.Text = "Favorite?";
            this.Favorite.UseVisualStyleBackColor = true;
            // 
            // Time
            // 
            this.Time.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Time.Location = new System.Drawing.Point(712, 575);
            this.Time.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Time.Name = "Time";
            this.Time.Size = new System.Drawing.Size(94, 33);
            this.Time.TabIndex = 53;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(798, 688);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 29);
            this.label1.TabIndex = 55;
            this.label1.Text = "Unit";
            // 
            // Unit_CB
            // 
            this.Unit_CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Unit_CB.FormattingEnabled = true;
            this.Unit_CB.Location = new System.Drawing.Point(732, 715);
            this.Unit_CB.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Unit_CB.Name = "Unit_CB";
            this.Unit_CB.Size = new System.Drawing.Size(184, 37);
            this.Unit_CB.TabIndex = 54;
            // 
            // Add_Recipe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(918, 938);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Unit_CB);
            this.Controls.Add(this.Time);
            this.Controls.Add(this.Favorite);
            this.Controls.Add(this.Save_PB);
            this.Controls.Add(this.Unit_Type);
            this.Controls.Add(this.Quanitity_Label);
            this.Controls.Add(this.Name_Label);
            this.Controls.Add(this.Cancel_PB);
            this.Controls.Add(this.Remove_PB);
            this.Controls.Add(this.Add_PB);
            this.Controls.Add(this.Unit_Type_CB);
            this.Controls.Add(this.Quantity_Numeric);
            this.Controls.Add(this.Name_TB);
            this.Controls.Add(this.Ingredients_Label);
            this.Controls.Add(this.Directions_Label);
            this.Controls.Add(this.Title_Label);
            this.Controls.Add(this.Description_Label);
            this.Controls.Add(this.Ingredients_Table);
            this.Controls.Add(this.Recipe_Directions_TB);
            this.Controls.Add(this.Recipe_Description_TB);
            this.Controls.Add(this.Recipe_Title_TB);
            this.Controls.Add(this.Note_Label);
            this.Controls.Add(this.Title);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Add_Recipe";
            this.Text = "Add_Recipe";
            ((System.ComponentModel.ISupportInitialize)(this.Quantity_Numeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ingredients_Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Time)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Save_PB;
        private System.Windows.Forms.Label Unit_Type;
        private System.Windows.Forms.Label Quanitity_Label;
        private System.Windows.Forms.Label Name_Label;
        private System.Windows.Forms.Button Cancel_PB;
        private System.Windows.Forms.Button Remove_PB;
        private System.Windows.Forms.Button Add_PB;
        private System.Windows.Forms.ComboBox Unit_Type_CB;
        private System.Windows.Forms.NumericUpDown Quantity_Numeric;
        private System.Windows.Forms.TextBox Name_TB;
        private System.Windows.Forms.Label Ingredients_Label;
        private System.Windows.Forms.Label Directions_Label;
        private System.Windows.Forms.Label Title_Label;
        private System.Windows.Forms.Label Description_Label;
        private System.Windows.Forms.DataGridView Ingredients_Table;
        private System.Windows.Forms.TextBox Recipe_Directions_TB;
        private System.Windows.Forms.TextBox Recipe_Description_TB;
        private System.Windows.Forms.TextBox Recipe_Title_TB;
        private System.Windows.Forms.Label Note_Label;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.RadioButton Favorite;
        private System.Windows.Forms.NumericUpDown Time;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox Unit_CB;
    }
}