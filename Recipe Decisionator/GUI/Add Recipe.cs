﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recipe_Decisionator
{
    public partial class Add_Recipe : Form
    {
        Recipe newRecipe;
        DataTable Ing;

        public Add_Recipe()
        {
            newRecipe = new Recipe();
            InitializeComponent();
            this.CenterToScreen();
            //create datatable for ingredients
            Ing = new DataTable();
            Ing.Columns.Add("Ingredient Name", typeof(string));
            Ing.Columns.Add("Quantity", typeof(double));
            Ing.Columns.Add("Unit", typeof(string));
           
            Ingredients_Table.DataSource = Ing;
            //bind to datagridview
        }

        private void Cancel_PB_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Save_PB_Click(object sender, EventArgs e)
        {
            //save code here
            //copy constructor to save table
            newRecipe.Name = Name_TB.Text;
            newRecipe.Description = Description_Label.Text;
            newRecipe.Directions = Directions_Label.Text;
            newRecipe.Favorite = Favorite.Checked;
            newRecipe.PrepTime = Convert.ToUInt32(Time.Value);

            foreach(DataRow DR in Ing) { }

            this.Close();
        }

        private void Add_PB_Click(object sender, EventArgs e)
        {
            DataRow entry = Ing.NewRow();
            entry["Ingredient Name"] = Name_TB.Text;
            entry["Unit"] = Unit_CB.Text;
            entry["Quantity"] = Quantity_Numeric.Value;
            Ing.Rows.Add(entry);
            //create a row with information entered
            //make sure all fields have values
        }

        private void Remove_PB_Click(object sender, EventArgs e)
        {
            if (Ingredients_Table.SelectedRows != null)
            {
                foreach (DataGridViewRow row in Ingredients_Table.SelectedRows)
                Ingredients_Table.Rows.Remove(row);
            }
            //remove row from table
            
            //Ingredients_Table.SelectedRows;
        }
    }
}
