﻿namespace Recipe_Decisionator
{
    partial class Recipe_Search_Results
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Back_PB = new System.Windows.Forms.Button();
            this.Title = new System.Windows.Forms.Label();
            this.Recipe_Results_TB = new System.Windows.Forms.TextBox();
            this.Selected_Recipe_CB = new System.Windows.Forms.ComboBox();
            this.Select_PB = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Back_PB
            // 
            this.Back_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Back_PB.Location = new System.Drawing.Point(599, 540);
            this.Back_PB.Name = "Back_PB";
            this.Back_PB.Size = new System.Drawing.Size(85, 35);
            this.Back_PB.TabIndex = 20;
            this.Back_PB.Text = "BACK";
            this.Back_PB.UseVisualStyleBackColor = true;
            this.Back_PB.Click += new System.EventHandler(this.Back_PB_Click);
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.Location = new System.Drawing.Point(116, 9);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(420, 42);
            this.Title.TabIndex = 19;
            this.Title.Text = "Recipe Search Results";
            // 
            // Recipe_Results_TB
            // 
            this.Recipe_Results_TB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Recipe_Results_TB.Location = new System.Drawing.Point(42, 91);
            this.Recipe_Results_TB.Multiline = true;
            this.Recipe_Results_TB.Name = "Recipe_Results_TB";
            this.Recipe_Results_TB.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Recipe_Results_TB.Size = new System.Drawing.Size(623, 410);
            this.Recipe_Results_TB.TabIndex = 21;
            // 
            // Selected_Recipe_CB
            // 
            this.Selected_Recipe_CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Selected_Recipe_CB.FormattingEnabled = true;
            this.Selected_Recipe_CB.Location = new System.Drawing.Point(42, 540);
            this.Selected_Recipe_CB.Name = "Selected_Recipe_CB";
            this.Selected_Recipe_CB.Size = new System.Drawing.Size(282, 26);
            this.Selected_Recipe_CB.TabIndex = 22;
            // 
            // Select_PB
            // 
            this.Select_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Select_PB.Location = new System.Drawing.Point(348, 535);
            this.Select_PB.Name = "Select_PB";
            this.Select_PB.Size = new System.Drawing.Size(85, 35);
            this.Select_PB.TabIndex = 23;
            this.Select_PB.Text = "Select";
            this.Select_PB.UseVisualStyleBackColor = true;
            this.Select_PB.Click += new System.EventHandler(this.Select_PB_Click);
            // 
            // Recipe_Search_Results
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(696, 587);
            this.Controls.Add(this.Select_PB);
            this.Controls.Add(this.Selected_Recipe_CB);
            this.Controls.Add(this.Recipe_Results_TB);
            this.Controls.Add(this.Back_PB);
            this.Controls.Add(this.Title);
            this.Name = "Recipe_Search_Results";
            this.Text = "Recipe_Search_Results";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Back_PB;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.TextBox Recipe_Results_TB;
        private System.Windows.Forms.ComboBox Selected_Recipe_CB;
        private System.Windows.Forms.Button Select_PB;
    }
}