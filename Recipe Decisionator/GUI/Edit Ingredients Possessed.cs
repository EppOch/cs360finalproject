﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recipe_Decisionator
{
    public partial class Edit_Ingredients_Possessed : Form
    {
        public Edit_Ingredients_Possessed()
        {
            InitializeComponent();
            //this.CenterToScreen();
            //this.Cursor = Cursors.WaitCursor;
            //retrieve datatable from the dataset for the current fridge contents
            //bind to gridview
        }

        private void Cancel_PB_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// When this event occurs the local ingredient name, unit, and quantity should be
        /// written to the table of ingredients, specifically they should be added to the fridge
        /// and the fridge object should be updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Add_PB_Click(object sender, EventArgs e)
        {
            //create the temporary ingredient to use
            QuantifiedIngredient TempIngredient = new QuantifiedIngredient(Name_TB.Text, GLOBALUnits[Name_Label.Text], Quantity.Value);
            //add to table

        }

        private void Remove_PB_Click(object sender, EventArgs e)
        {
            //remove from table
        }

        private void Save_PB_Click(object sender, EventArgs e)
        {
            //save
            //Success?
        }
    }
}
