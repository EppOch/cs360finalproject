﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recipe_Decisionator
{
    public partial class Recipe_Display : Form
    {
        public Recipe_Display(Recipe SelectedRecipe)
        {
            InitializeComponent();
            this.CenterToScreen();
            //ADD DELETE BUTTON TO DELETE THE DISPLAYED RECIPE
            //Use the tostringfullprint function to display recipe
        }

        private void Add_Recipe_to_Shopping_List_PB_Click(object sender, EventArgs e)
        {
            var myForm = new Ingredients_Added(); //or error ingredients not added depending on feedback from the function
            myForm.TopMost = true;
            myForm.ShowDialog();
            this.Show();
        }

        private void Back_PB_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Modify_Recipe_PB_Click(object sender, EventArgs e)
        {
            Recipe modRecipe = new Recipe();
            //pass recipe
            this.Hide();
            var myForm = new Modify_Recipe(modRecipe); //or error ingredients not added depending on feedback from the function
            myForm.TopMost = true;
            myForm.ShowDialog();
            this.Show();
        }

        private void Delete_PB_Click(object sender, EventArgs e)
        {
            //delete function
            //success?????
        }
    }
}
