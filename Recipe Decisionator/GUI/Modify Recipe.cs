﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recipe_Decisionator
{
    public partial class Modify_Recipe : Form
    {
        public Modify_Recipe(Recipe SelectedRecipe)
        {
            InitializeComponent();
            this.CenterToScreen();
            //preload recipe information
        }

        private void Save_Click(object sender, EventArgs e)
        {
            //put save code here

            this.Close();
        }

        private void Cancel_PB_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
