﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recipe_Decisionator
{
    public partial class Show_Edit_ShoppingList : Form
    {
        public Show_Edit_ShoppingList()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        private void Back_PB_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Minus_Fridge_Check_CheckedChanged(object sender, EventArgs e)
        {
            //two if statements display list based off checked value
            //Minus_Fridge_Check.Checked;
            //keep old list of ingredients to display when unchecked
        }

        private void Save_PB_Click(object sender, EventArgs e)
        {
            //pass Datatable of ingredients for save/print
        }

        private void Add_PB_Click(object sender, EventArgs e)
        {
            //add to table
        }

        private void Remove_PB_Click(object sender, EventArgs e)
        {
            //remove line from table
        }

        private void Save_To_File_PB_Click(object sender, EventArgs e)
        {
            //save to file
        }

        private void Print_PB_Click(object sender, EventArgs e)
        {
            //print
        }
    }
}
