﻿namespace Recipe_Decisionator
{
    partial class Modify_Recipe_Database
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Recipe_Title_TB = new System.Windows.Forms.TextBox();
            this.Note_Label = new System.Windows.Forms.Label();
            this.Title = new System.Windows.Forms.Label();
            this.Or_Label = new System.Windows.Forms.Label();
            this.Add_New_Recipe_PB = new System.Windows.Forms.Button();
            this.Back_PB = new System.Windows.Forms.Button();
            this.Search_PB = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Recipe_Title_TB
            // 
            this.Recipe_Title_TB.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Recipe_Title_TB.Location = new System.Drawing.Point(41, 125);
            this.Recipe_Title_TB.Name = "Recipe_Title_TB";
            this.Recipe_Title_TB.Size = new System.Drawing.Size(407, 29);
            this.Recipe_Title_TB.TabIndex = 0;
            // 
            // Note_Label
            // 
            this.Note_Label.AutoSize = true;
            this.Note_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Note_Label.Location = new System.Drawing.Point(38, 104);
            this.Note_Label.Name = "Note_Label";
            this.Note_Label.Size = new System.Drawing.Size(410, 18);
            this.Note_Label.TabIndex = 1;
            this.Note_Label.Text = "Note: Please enter the title of the recipe you would like to find.";
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.Location = new System.Drawing.Point(79, 18);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(448, 42);
            this.Title.TabIndex = 11;
            this.Title.Text = "Modify Recipe Database";
            // 
            // Or_Label
            // 
            this.Or_Label.AutoSize = true;
            this.Or_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Or_Label.Location = new System.Drawing.Point(288, 172);
            this.Or_Label.Name = "Or_Label";
            this.Or_Label.Size = new System.Drawing.Size(31, 18);
            this.Or_Label.TabIndex = 12;
            this.Or_Label.Text = "OR";
            // 
            // Add_New_Recipe_PB
            // 
            this.Add_New_Recipe_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Add_New_Recipe_PB.Location = new System.Drawing.Point(193, 216);
            this.Add_New_Recipe_PB.Name = "Add_New_Recipe_PB";
            this.Add_New_Recipe_PB.Size = new System.Drawing.Size(221, 56);
            this.Add_New_Recipe_PB.TabIndex = 13;
            this.Add_New_Recipe_PB.Text = "Add New Recipe";
            this.Add_New_Recipe_PB.UseVisualStyleBackColor = false;
            this.Add_New_Recipe_PB.Click += new System.EventHandler(this.Add_New_Recipe_PB_Click);
            // 
            // Back_PB
            // 
            this.Back_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Back_PB.Location = new System.Drawing.Point(481, 321);
            this.Back_PB.Name = "Back_PB";
            this.Back_PB.Size = new System.Drawing.Size(85, 35);
            this.Back_PB.TabIndex = 18;
            this.Back_PB.Text = "BACK";
            this.Back_PB.UseVisualStyleBackColor = true;
            this.Back_PB.Click += new System.EventHandler(this.Back_PB_Click);
            // 
            // Search_PB
            // 
            this.Search_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Search_PB.Location = new System.Drawing.Point(481, 122);
            this.Search_PB.Name = "Search_PB";
            this.Search_PB.Size = new System.Drawing.Size(85, 35);
            this.Search_PB.TabIndex = 19;
            this.Search_PB.Text = "Search";
            this.Search_PB.UseVisualStyleBackColor = true;
            this.Search_PB.Click += new System.EventHandler(this.Search_PB_Click);
            // 
            // Modify_Recipe_Database
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 368);
            this.Controls.Add(this.Search_PB);
            this.Controls.Add(this.Back_PB);
            this.Controls.Add(this.Add_New_Recipe_PB);
            this.Controls.Add(this.Or_Label);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.Note_Label);
            this.Controls.Add(this.Recipe_Title_TB);
            this.Name = "Modify_Recipe_Database";
            this.Text = "Modify_Recipe_Database";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Recipe_Title_TB;
        private System.Windows.Forms.Label Note_Label;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label Or_Label;
        private System.Windows.Forms.Button Add_New_Recipe_PB;
        private System.Windows.Forms.Button Back_PB;
        private System.Windows.Forms.Button Search_PB;
    }
}