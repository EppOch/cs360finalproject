﻿namespace Recipe_Decisionator
{
    partial class Show_Edit_ShoppingList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Unit_Label = new System.Windows.Forms.Label();
            this.Quanitity_Label = new System.Windows.Forms.Label();
            this.Name_Label = new System.Windows.Forms.Label();
            this.Back_PB = new System.Windows.Forms.Button();
            this.Remove_PB = new System.Windows.Forms.Button();
            this.Add_PB = new System.Windows.Forms.Button();
            this.Unit = new System.Windows.Forms.ComboBox();
            this.Quantity = new System.Windows.Forms.NumericUpDown();
            this.Name_TB = new System.Windows.Forms.TextBox();
            this.List_Ingredients_Label = new System.Windows.Forms.Label();
            this.Title = new System.Windows.Forms.Label();
            this.Minus_Fridge_Check = new System.Windows.Forms.CheckBox();
            this.Save_PB = new System.Windows.Forms.Button();
            this.Ingredients_Table = new System.Windows.Forms.DataGridView();
            this.Ingredient = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Print_PB = new System.Windows.Forms.Button();
            this.Save_To_File_PB = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Quantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ingredients_Table)).BeginInit();
            this.SuspendLayout();
            // 
            // Unit_Label
            // 
            this.Unit_Label.AutoSize = true;
            this.Unit_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Unit_Label.Location = new System.Drawing.Point(497, 376);
            this.Unit_Label.Name = "Unit_Label";
            this.Unit_Label.Size = new System.Drawing.Size(34, 18);
            this.Unit_Label.TabIndex = 23;
            this.Unit_Label.Text = "Unit";
            // 
            // Quanitity_Label
            // 
            this.Quanitity_Label.AutoSize = true;
            this.Quanitity_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Quanitity_Label.Location = new System.Drawing.Point(338, 376);
            this.Quanitity_Label.Name = "Quanitity_Label";
            this.Quanitity_Label.Size = new System.Drawing.Size(65, 18);
            this.Quanitity_Label.TabIndex = 22;
            this.Quanitity_Label.Text = "Quantitiy";
            // 
            // Name_Label
            // 
            this.Name_Label.AutoSize = true;
            this.Name_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name_Label.Location = new System.Drawing.Point(71, 376);
            this.Name_Label.Name = "Name_Label";
            this.Name_Label.Size = new System.Drawing.Size(48, 18);
            this.Name_Label.TabIndex = 21;
            this.Name_Label.Text = "Name";
            // 
            // Back_PB
            // 
            this.Back_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Back_PB.Location = new System.Drawing.Point(541, 564);
            this.Back_PB.Name = "Back_PB";
            this.Back_PB.Size = new System.Drawing.Size(85, 35);
            this.Back_PB.TabIndex = 20;
            this.Back_PB.Text = "BACK";
            this.Back_PB.UseVisualStyleBackColor = true;
            this.Back_PB.Click += new System.EventHandler(this.Back_PB_Click);
            // 
            // Remove_PB
            // 
            this.Remove_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Remove_PB.Location = new System.Drawing.Point(378, 441);
            this.Remove_PB.Name = "Remove_PB";
            this.Remove_PB.Size = new System.Drawing.Size(85, 35);
            this.Remove_PB.TabIndex = 19;
            this.Remove_PB.Text = "Remove";
            this.Remove_PB.UseVisualStyleBackColor = true;
            this.Remove_PB.Click += new System.EventHandler(this.Remove_PB_Click);
            // 
            // Add_PB
            // 
            this.Add_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Add_PB.Location = new System.Drawing.Point(225, 441);
            this.Add_PB.Name = "Add_PB";
            this.Add_PB.Size = new System.Drawing.Size(85, 35);
            this.Add_PB.TabIndex = 18;
            this.Add_PB.Text = "Add";
            this.Add_PB.UseVisualStyleBackColor = true;
            this.Add_PB.Click += new System.EventHandler(this.Add_PB_Click);
            // 
            // Unit
            // 
            this.Unit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Unit.FormattingEnabled = true;
            this.Unit.Location = new System.Drawing.Point(449, 395);
            this.Unit.Name = "Unit";
            this.Unit.Size = new System.Drawing.Size(124, 26);
            this.Unit.TabIndex = 17;
            // 
            // Quantity
            // 
            this.Quantity.AllowDrop = true;
            this.Quantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Quantity.Location = new System.Drawing.Point(344, 395);
            this.Quantity.Name = "Quantity";
            this.Quantity.Size = new System.Drawing.Size(55, 24);
            this.Quantity.TabIndex = 16;
            // 
            // Name_TB
            // 
            this.Name_TB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name_TB.Location = new System.Drawing.Point(73, 395);
            this.Name_TB.Name = "Name_TB";
            this.Name_TB.Size = new System.Drawing.Size(208, 24);
            this.Name_TB.TabIndex = 15;
            // 
            // List_Ingredients_Label
            // 
            this.List_Ingredients_Label.AutoSize = true;
            this.List_Ingredients_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.List_Ingredients_Label.Location = new System.Drawing.Point(149, 83);
            this.List_Ingredients_Label.Name = "List_Ingredients_Label";
            this.List_Ingredients_Label.Size = new System.Drawing.Size(283, 18);
            this.List_Ingredients_Label.TabIndex = 14;
            this.List_Ingredients_Label.Text = "List of items currently on the shopping list.";
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.Location = new System.Drawing.Point(205, 9);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(258, 42);
            this.Title.TabIndex = 13;
            this.Title.Text = "Shopping List";
            // 
            // Minus_Fridge_Check
            // 
            this.Minus_Fridge_Check.AutoSize = true;
            this.Minus_Fridge_Check.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Minus_Fridge_Check.Location = new System.Drawing.Point(45, 496);
            this.Minus_Fridge_Check.Name = "Minus_Fridge_Check";
            this.Minus_Fridge_Check.Size = new System.Drawing.Size(288, 22);
            this.Minus_Fridge_Check.TabIndex = 24;
            this.Minus_Fridge_Check.Text = "Subtract Ingredients Already Possessed";
            this.Minus_Fridge_Check.UseVisualStyleBackColor = true;
            this.Minus_Fridge_Check.CheckedChanged += new System.EventHandler(this.Minus_Fridge_Check_CheckedChanged);
            // 
            // Save_PB
            // 
            this.Save_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Save_PB.Location = new System.Drawing.Point(427, 564);
            this.Save_PB.Name = "Save_PB";
            this.Save_PB.Size = new System.Drawing.Size(85, 35);
            this.Save_PB.TabIndex = 25;
            this.Save_PB.Text = "SAVE";
            this.Save_PB.UseVisualStyleBackColor = true;
            this.Save_PB.Click += new System.EventHandler(this.Save_PB_Click);
            // 
            // Ingredients_Table
            // 
            this.Ingredients_Table.AllowUserToDeleteRows = false;
            this.Ingredients_Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Ingredients_Table.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Ingredient,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.Ingredients_Table.Location = new System.Drawing.Point(152, 104);
            this.Ingredients_Table.MultiSelect = false;
            this.Ingredients_Table.Name = "Ingredients_Table";
            this.Ingredients_Table.ReadOnly = true;
            this.Ingredients_Table.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Ingredients_Table.Size = new System.Drawing.Size(345, 251);
            this.Ingredients_Table.TabIndex = 26;
            // 
            // Ingredient
            // 
            this.Ingredient.HeaderText = "Ingredient";
            this.Ingredient.Name = "Ingredient";
            this.Ingredient.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Unit";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // Print_PB
            // 
            this.Print_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Print_PB.Location = new System.Drawing.Point(152, 564);
            this.Print_PB.Name = "Print_PB";
            this.Print_PB.Size = new System.Drawing.Size(85, 35);
            this.Print_PB.TabIndex = 27;
            this.Print_PB.Text = "PRINT";
            this.Print_PB.UseVisualStyleBackColor = true;
            this.Print_PB.Click += new System.EventHandler(this.Print_PB_Click);
            // 
            // Save_To_File_PB
            // 
            this.Save_To_File_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Save_To_File_PB.Location = new System.Drawing.Point(274, 564);
            this.Save_To_File_PB.Name = "Save_To_File_PB";
            this.Save_To_File_PB.Size = new System.Drawing.Size(125, 35);
            this.Save_To_File_PB.TabIndex = 28;
            this.Save_To_File_PB.Text = "SAVE TO FILE";
            this.Save_To_File_PB.UseVisualStyleBackColor = true;
            this.Save_To_File_PB.Click += new System.EventHandler(this.Save_To_File_PB_Click);
            // 
            // Show_Edit_ShoppingList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 611);
            this.Controls.Add(this.Save_To_File_PB);
            this.Controls.Add(this.Print_PB);
            this.Controls.Add(this.Ingredients_Table);
            this.Controls.Add(this.Save_PB);
            this.Controls.Add(this.Minus_Fridge_Check);
            this.Controls.Add(this.Unit_Label);
            this.Controls.Add(this.Quanitity_Label);
            this.Controls.Add(this.Name_Label);
            this.Controls.Add(this.Back_PB);
            this.Controls.Add(this.Remove_PB);
            this.Controls.Add(this.Add_PB);
            this.Controls.Add(this.Unit);
            this.Controls.Add(this.Quantity);
            this.Controls.Add(this.Name_TB);
            this.Controls.Add(this.List_Ingredients_Label);
            this.Controls.Add(this.Title);
            this.Name = "Show_Edit_ShoppingList";
            this.Text = "Show_Edit_ShoppingList";
            ((System.ComponentModel.ISupportInitialize)(this.Quantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ingredients_Table)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Unit_Label;
        private System.Windows.Forms.Label Quanitity_Label;
        private System.Windows.Forms.Label Name_Label;
        private System.Windows.Forms.Button Back_PB;
        private System.Windows.Forms.Button Remove_PB;
        private System.Windows.Forms.Button Add_PB;
        private System.Windows.Forms.ComboBox Unit;
        private System.Windows.Forms.NumericUpDown Quantity;
        private System.Windows.Forms.TextBox Name_TB;
        private System.Windows.Forms.Label List_Ingredients_Label;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.CheckBox Minus_Fridge_Check;
        private System.Windows.Forms.Button Save_PB;
        private System.Windows.Forms.DataGridView Ingredients_Table;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ingredient;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Button Print_PB;
        private System.Windows.Forms.Button Save_To_File_PB;
    }
}