﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recipe_Decisionator
{
    public partial class Modify_Recipe_Database : Form
    {
        public Modify_Recipe_Database()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        private void Add_New_Recipe_PB_Click(object sender, EventArgs e)
        {
            this.Hide();
            var myForm = new Add_Recipe();
            myForm.TopMost = true;
            myForm.ShowDialog();
            this.Show();
        }

        private void Back_PB_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Search_PB_Click(object sender, EventArgs e)
        {
            List<Recipe> results = new List<Recipe>();
            //run search 
            this.Hide();
            var myForm = new Recipe_Search_Results(results);
            myForm.TopMost = true;
            myForm.ShowDialog();
            this.Show();
        }
    }
}
