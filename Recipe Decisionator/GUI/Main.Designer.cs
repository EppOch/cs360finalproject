﻿namespace Recipe_Decisionator
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Search_Recipe_DB_PB = new System.Windows.Forms.Button();
            this.Modify_Recipe_DB_PB = new System.Windows.Forms.Button();
            this.Modify_Ingredients_Possessed_PB = new System.Windows.Forms.Button();
            this.Find_Recipe_Based_Off_Ingredients_PB = new System.Windows.Forms.Button();
            this.Generate_Shopping_List_PB = new System.Windows.Forms.Button();
            this.Title = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Search_Recipe_DB_PB
            // 
            this.Search_Recipe_DB_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Search_Recipe_DB_PB.Location = new System.Drawing.Point(151, 200);
            this.Search_Recipe_DB_PB.Name = "Search_Recipe_DB_PB";
            this.Search_Recipe_DB_PB.Size = new System.Drawing.Size(130, 80);
            this.Search_Recipe_DB_PB.TabIndex = 0;
            this.Search_Recipe_DB_PB.Text = "Search Recipe Database";
            this.Search_Recipe_DB_PB.UseVisualStyleBackColor = true;
            this.Search_Recipe_DB_PB.Click += new System.EventHandler(this.Search_Recipe_DB_PB_Click);
            // 
            // Modify_Recipe_DB_PB
            // 
            this.Modify_Recipe_DB_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Modify_Recipe_DB_PB.Location = new System.Drawing.Point(354, 200);
            this.Modify_Recipe_DB_PB.Name = "Modify_Recipe_DB_PB";
            this.Modify_Recipe_DB_PB.Size = new System.Drawing.Size(130, 80);
            this.Modify_Recipe_DB_PB.TabIndex = 1;
            this.Modify_Recipe_DB_PB.Text = "Modify Recipe Database";
            this.Modify_Recipe_DB_PB.UseVisualStyleBackColor = true;
            this.Modify_Recipe_DB_PB.Click += new System.EventHandler(this.Modify_Recipe_DB_PB_Click);
            // 
            // Modify_Ingredients_Possessed_PB
            // 
            this.Modify_Ingredients_Possessed_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Modify_Ingredients_Possessed_PB.Location = new System.Drawing.Point(52, 319);
            this.Modify_Ingredients_Possessed_PB.Name = "Modify_Ingredients_Possessed_PB";
            this.Modify_Ingredients_Possessed_PB.Size = new System.Drawing.Size(130, 80);
            this.Modify_Ingredients_Possessed_PB.TabIndex = 2;
            this.Modify_Ingredients_Possessed_PB.Text = "Modify Ingredients Possessed";
            this.Modify_Ingredients_Possessed_PB.UseVisualStyleBackColor = true;
            this.Modify_Ingredients_Possessed_PB.Click += new System.EventHandler(this.Modify_Ingredients_Possessed_PB_Click);
            // 
            // Find_Recipe_Based_Off_Ingredients_PB
            // 
            this.Find_Recipe_Based_Off_Ingredients_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Find_Recipe_Based_Off_Ingredients_PB.Location = new System.Drawing.Point(248, 319);
            this.Find_Recipe_Based_Off_Ingredients_PB.Name = "Find_Recipe_Based_Off_Ingredients_PB";
            this.Find_Recipe_Based_Off_Ingredients_PB.Size = new System.Drawing.Size(130, 80);
            this.Find_Recipe_Based_Off_Ingredients_PB.TabIndex = 3;
            this.Find_Recipe_Based_Off_Ingredients_PB.Text = "Find Recipes Based Off Ingredients";
            this.Find_Recipe_Based_Off_Ingredients_PB.UseVisualStyleBackColor = true;
            this.Find_Recipe_Based_Off_Ingredients_PB.Click += new System.EventHandler(this.Find_Recipe_Based_Off_Ingredients_PB_Click);
            // 
            // Generate_Shopping_List_PB
            // 
            this.Generate_Shopping_List_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Generate_Shopping_List_PB.Location = new System.Drawing.Point(448, 319);
            this.Generate_Shopping_List_PB.Name = "Generate_Shopping_List_PB";
            this.Generate_Shopping_List_PB.Size = new System.Drawing.Size(130, 80);
            this.Generate_Shopping_List_PB.TabIndex = 4;
            this.Generate_Shopping_List_PB.Text = "Generate Shopping List";
            this.Generate_Shopping_List_PB.UseVisualStyleBackColor = true;
            this.Generate_Shopping_List_PB.Click += new System.EventHandler(this.Generate_Shopping_List_PB_Click);
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.Location = new System.Drawing.Point(132, 28);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(372, 42);
            this.Title.TabIndex = 5;
            this.Title.Text = "Recipe Decisionator";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(634, 411);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.Generate_Shopping_List_PB);
            this.Controls.Add(this.Find_Recipe_Based_Off_Ingredients_PB);
            this.Controls.Add(this.Modify_Ingredients_Possessed_PB);
            this.Controls.Add(this.Modify_Recipe_DB_PB);
            this.Controls.Add(this.Search_Recipe_DB_PB);
            this.Name = "Main";
            this.Text = "Main";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Search_Recipe_DB_PB;
        private System.Windows.Forms.Button Modify_Recipe_DB_PB;
        private System.Windows.Forms.Button Modify_Ingredients_Possessed_PB;
        private System.Windows.Forms.Button Find_Recipe_Based_Off_Ingredients_PB;
        private System.Windows.Forms.Button Generate_Shopping_List_PB;
        private System.Windows.Forms.Label Title;
    }
}