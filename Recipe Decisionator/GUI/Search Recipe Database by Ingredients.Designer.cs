﻿namespace Recipe_Decisionator.GUI
{
    partial class Search_Recipe_Database_by_Ingredients
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Search_Automatically_PB = new System.Windows.Forms.Button();
            this.Quanitity_Label = new System.Windows.Forms.Label();
            this.Name_Label = new System.Windows.Forms.Label();
            this.Remove_PB = new System.Windows.Forms.Button();
            this.Add_PB = new System.Windows.Forms.Button();
            this.Unit_CB = new System.Windows.Forms.ComboBox();
            this.Quantity_IN = new System.Windows.Forms.NumericUpDown();
            this.Name_TB = new System.Windows.Forms.TextBox();
            this.Back_PB = new System.Windows.Forms.Button();
            this.Unit_Label = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ingredients = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Or_Label = new System.Windows.Forms.Label();
            this.Title = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Search_PB = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Quantity_IN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // Search_Automatically_PB
            // 
            this.Search_Automatically_PB.Location = new System.Drawing.Point(208, 121);
            this.Search_Automatically_PB.Margin = new System.Windows.Forms.Padding(4);
            this.Search_Automatically_PB.Name = "Search_Automatically_PB";
            this.Search_Automatically_PB.Size = new System.Drawing.Size(211, 83);
            this.Search_Automatically_PB.TabIndex = 0;
            this.Search_Automatically_PB.Text = "Search Recipe Database by Known Ingredients";
            this.Search_Automatically_PB.UseVisualStyleBackColor = true;
            this.Search_Automatically_PB.Click += new System.EventHandler(this.Search_Automatically_PB_Click);
            // 
            // Quanitity_Label
            // 
            this.Quanitity_Label.AutoSize = true;
            this.Quanitity_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Quanitity_Label.Location = new System.Drawing.Point(321, 414);
            this.Quanitity_Label.Name = "Quanitity_Label";
            this.Quanitity_Label.Size = new System.Drawing.Size(65, 18);
            this.Quanitity_Label.TabIndex = 17;
            this.Quanitity_Label.Text = "Quantitiy";
            // 
            // Name_Label
            // 
            this.Name_Label.AutoSize = true;
            this.Name_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name_Label.Location = new System.Drawing.Point(54, 414);
            this.Name_Label.Name = "Name_Label";
            this.Name_Label.Size = new System.Drawing.Size(48, 18);
            this.Name_Label.TabIndex = 16;
            this.Name_Label.Text = "Name";
            // 
            // Remove_PB
            // 
            this.Remove_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Remove_PB.Location = new System.Drawing.Point(244, 468);
            this.Remove_PB.Name = "Remove_PB";
            this.Remove_PB.Size = new System.Drawing.Size(85, 35);
            this.Remove_PB.TabIndex = 15;
            this.Remove_PB.Text = "Remove";
            this.Remove_PB.UseVisualStyleBackColor = true;
            // 
            // Add_PB
            // 
            this.Add_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Add_PB.Location = new System.Drawing.Point(101, 468);
            this.Add_PB.Name = "Add_PB";
            this.Add_PB.Size = new System.Drawing.Size(85, 35);
            this.Add_PB.TabIndex = 14;
            this.Add_PB.Text = "Add";
            this.Add_PB.UseVisualStyleBackColor = true;
            // 
            // Unit_CB
            // 
            this.Unit_CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Unit_CB.FormattingEnabled = true;
            this.Unit_CB.Location = new System.Drawing.Point(432, 433);
            this.Unit_CB.Name = "Unit_CB";
            this.Unit_CB.Size = new System.Drawing.Size(124, 26);
            this.Unit_CB.TabIndex = 13;
            // 
            // Quantity_IN
            // 
            this.Quantity_IN.AllowDrop = true;
            this.Quantity_IN.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Quantity_IN.Location = new System.Drawing.Point(327, 433);
            this.Quantity_IN.Name = "Quantity_IN";
            this.Quantity_IN.Size = new System.Drawing.Size(55, 24);
            this.Quantity_IN.TabIndex = 12;
            // 
            // Name_TB
            // 
            this.Name_TB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name_TB.Location = new System.Drawing.Point(56, 433);
            this.Name_TB.Name = "Name_TB";
            this.Name_TB.Size = new System.Drawing.Size(208, 24);
            this.Name_TB.TabIndex = 11;
            // 
            // Back_PB
            // 
            this.Back_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Back_PB.Location = new System.Drawing.Point(529, 517);
            this.Back_PB.Name = "Back_PB";
            this.Back_PB.Size = new System.Drawing.Size(85, 35);
            this.Back_PB.TabIndex = 18;
            this.Back_PB.Text = "BACK";
            this.Back_PB.UseVisualStyleBackColor = true;
            this.Back_PB.Click += new System.EventHandler(this.Back_PB_Click);
            // 
            // Unit_Label
            // 
            this.Unit_Label.AutoSize = true;
            this.Unit_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Unit_Label.Location = new System.Drawing.Point(478, 412);
            this.Unit_Label.Name = "Unit_Label";
            this.Unit_Label.Size = new System.Drawing.Size(34, 18);
            this.Unit_Label.TabIndex = 19;
            this.Unit_Label.Text = "Unit";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Quantity,
            this.Ingredients,
            this.Unit});
            this.dataGridView1.Location = new System.Drawing.Point(140, 241);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(347, 170);
            this.dataGridView1.TabIndex = 20;
            // 
            // Quantity
            // 
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.Name = "Quantity";
            this.Quantity.ReadOnly = true;
            // 
            // Ingredients
            // 
            this.Ingredients.HeaderText = "Ingredients";
            this.Ingredients.Name = "Ingredients";
            this.Ingredients.ReadOnly = true;
            // 
            // Unit
            // 
            this.Unit.HeaderText = "Unit";
            this.Unit.Name = "Unit";
            this.Unit.ReadOnly = true;
            // 
            // Or_Label
            // 
            this.Or_Label.AutoSize = true;
            this.Or_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Or_Label.Location = new System.Drawing.Point(298, 214);
            this.Or_Label.Name = "Or_Label";
            this.Or_Label.Size = new System.Drawing.Size(31, 18);
            this.Or_Label.TabIndex = 21;
            this.Or_Label.Text = "OR";
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.Location = new System.Drawing.Point(175, 51);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(277, 42);
            this.Title.TabIndex = 22;
            this.Title.Text = " by Ingredients";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(85, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(457, 42);
            this.label1.TabIndex = 23;
            this.label1.Text = "Search Recipe Database";
            // 
            // Search_PB
            // 
            this.Search_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Search_PB.Location = new System.Drawing.Point(381, 468);
            this.Search_PB.Name = "Search_PB";
            this.Search_PB.Size = new System.Drawing.Size(85, 35);
            this.Search_PB.TabIndex = 24;
            this.Search_PB.Text = "SEARCH";
            this.Search_PB.UseVisualStyleBackColor = true;
            this.Search_PB.Click += new System.EventHandler(this.Search_PB_Click);
            // 
            // Search_Recipe_Database_by_Ingredients
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 564);
            this.Controls.Add(this.Search_PB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.Or_Label);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.Unit_Label);
            this.Controls.Add(this.Back_PB);
            this.Controls.Add(this.Quanitity_Label);
            this.Controls.Add(this.Name_Label);
            this.Controls.Add(this.Remove_PB);
            this.Controls.Add(this.Add_PB);
            this.Controls.Add(this.Unit_CB);
            this.Controls.Add(this.Quantity_IN);
            this.Controls.Add(this.Name_TB);
            this.Controls.Add(this.Search_Automatically_PB);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Search_Recipe_Database_by_Ingredients";
            this.Text = "Search_Recipe_Database_by_Ingredients";
            ((System.ComponentModel.ISupportInitialize)(this.Quantity_IN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Search_Automatically_PB;
        private System.Windows.Forms.Label Quanitity_Label;
        private System.Windows.Forms.Label Name_Label;
        private System.Windows.Forms.Button Remove_PB;
        private System.Windows.Forms.Button Add_PB;
        private System.Windows.Forms.ComboBox Unit_CB;
        private System.Windows.Forms.NumericUpDown Quantity_IN;
        private System.Windows.Forms.TextBox Name_TB;
        private System.Windows.Forms.Button Back_PB;
        private System.Windows.Forms.Label Unit_Label;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ingredients;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unit;
        private System.Windows.Forms.Label Or_Label;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Search_PB;
    }
}