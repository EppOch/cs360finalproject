﻿namespace Recipe_Decisionator
{
    partial class Recipe_Display
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Title = new System.Windows.Forms.Label();
            this.Recipe_Information_TB = new System.Windows.Forms.TextBox();
            this.Add_Recipe_to_Shopping_List_PB = new System.Windows.Forms.Button();
            this.Back_PB = new System.Windows.Forms.Button();
            this.Modify_Recipe_PB = new System.Windows.Forms.Button();
            this.Delete_PB = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.Location = new System.Drawing.Point(204, 9);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(141, 42);
            this.Title.TabIndex = 0;
            this.Title.Text = "Recipe";
            // 
            // Recipe_Information_TB
            // 
            this.Recipe_Information_TB.Location = new System.Drawing.Point(21, 66);
            this.Recipe_Information_TB.Multiline = true;
            this.Recipe_Information_TB.Name = "Recipe_Information_TB";
            this.Recipe_Information_TB.Size = new System.Drawing.Size(515, 286);
            this.Recipe_Information_TB.TabIndex = 1;
            // 
            // Add_Recipe_to_Shopping_List_PB
            // 
            this.Add_Recipe_to_Shopping_List_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Add_Recipe_to_Shopping_List_PB.Location = new System.Drawing.Point(167, 370);
            this.Add_Recipe_to_Shopping_List_PB.Name = "Add_Recipe_to_Shopping_List_PB";
            this.Add_Recipe_to_Shopping_List_PB.Size = new System.Drawing.Size(111, 66);
            this.Add_Recipe_to_Shopping_List_PB.TabIndex = 2;
            this.Add_Recipe_to_Shopping_List_PB.Text = "Add Recipe Ingredients to Shopping List";
            this.Add_Recipe_to_Shopping_List_PB.UseVisualStyleBackColor = true;
            this.Add_Recipe_to_Shopping_List_PB.Click += new System.EventHandler(this.Add_Recipe_to_Shopping_List_PB_Click);
            // 
            // Back_PB
            // 
            this.Back_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Back_PB.Location = new System.Drawing.Point(451, 401);
            this.Back_PB.Name = "Back_PB";
            this.Back_PB.Size = new System.Drawing.Size(85, 35);
            this.Back_PB.TabIndex = 9;
            this.Back_PB.Text = "BACK";
            this.Back_PB.UseVisualStyleBackColor = true;
            this.Back_PB.Click += new System.EventHandler(this.Back_PB_Click);
            // 
            // Modify_Recipe_PB
            // 
            this.Modify_Recipe_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Modify_Recipe_PB.Location = new System.Drawing.Point(21, 370);
            this.Modify_Recipe_PB.Name = "Modify_Recipe_PB";
            this.Modify_Recipe_PB.Size = new System.Drawing.Size(115, 66);
            this.Modify_Recipe_PB.TabIndex = 2;
            this.Modify_Recipe_PB.Text = "Modify Recipe";
            this.Modify_Recipe_PB.UseVisualStyleBackColor = true;
            this.Modify_Recipe_PB.Click += new System.EventHandler(this.Modify_Recipe_PB_Click);
            // 
            // Delete_PB
            // 
            this.Delete_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Delete_PB.Location = new System.Drawing.Point(307, 370);
            this.Delete_PB.Name = "Delete_PB";
            this.Delete_PB.Size = new System.Drawing.Size(108, 66);
            this.Delete_PB.TabIndex = 10;
            this.Delete_PB.Text = "Delete Recipe";
            this.Delete_PB.UseVisualStyleBackColor = true;
            this.Delete_PB.Click += new System.EventHandler(this.Delete_PB_Click);
            // 
            // Recipe_Display
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 447);
            this.Controls.Add(this.Delete_PB);
            this.Controls.Add(this.Back_PB);
            this.Controls.Add(this.Modify_Recipe_PB);
            this.Controls.Add(this.Add_Recipe_to_Shopping_List_PB);
            this.Controls.Add(this.Recipe_Information_TB);
            this.Controls.Add(this.Title);
            this.Name = "Recipe_Display";
            this.Text = "Recipe_Display";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.TextBox Recipe_Information_TB;
        private System.Windows.Forms.Button Add_Recipe_to_Shopping_List_PB;
        private System.Windows.Forms.Button Back_PB;
        private System.Windows.Forms.Button Modify_Recipe_PB;
        private System.Windows.Forms.Button Delete_PB;
    }
}