﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recipe_Decisionator.GUI
{
    public partial class Search_Recipe_Database_by_Ingredients : Form
    {
        public Search_Recipe_Database_by_Ingredients()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        private void Back_PB_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Search_PB_Click(object sender, EventArgs e)
        {
            List<Recipe> recList = new List<Recipe>();
            //run search by rendom ingredients table
            //Success?
            this.Hide();
            var myForm = new Recipe_Search_Results(recList);
            myForm.TopMost = true;
            myForm.ShowDialog();
            this.Show();
        }

        private void Search_Automatically_PB_Click(object sender, EventArgs e)
        {
            List<Recipe> results = new List<Recipe>();
            //run search by known ingredients
            //Success?
            this.Hide();
            var myForm = new Recipe_Search_Results(results);
            myForm.TopMost = true;
            myForm.ShowDialog();
            this.Show();
        }
    }
}
