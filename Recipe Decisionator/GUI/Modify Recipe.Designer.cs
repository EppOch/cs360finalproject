﻿namespace Recipe_Decisionator
{
    partial class Modify_Recipe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Title = new System.Windows.Forms.Label();
            this.Note_Label = new System.Windows.Forms.Label();
            this.Recipe_Title_TB = new System.Windows.Forms.TextBox();
            this.Recipe_Description_TB = new System.Windows.Forms.TextBox();
            this.Recipe_Directions_TB = new System.Windows.Forms.TextBox();
            this.Ingredients_Table = new System.Windows.Forms.DataGridView();
            this.Ingredient = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Title_Label = new System.Windows.Forms.Label();
            this.Description_Label = new System.Windows.Forms.Label();
            this.Directions_Label = new System.Windows.Forms.Label();
            this.Ingredients_Label = new System.Windows.Forms.Label();
            this.Unit_Label = new System.Windows.Forms.Label();
            this.Quanitity_Label = new System.Windows.Forms.Label();
            this.Name_Label = new System.Windows.Forms.Label();
            this.Cancel_PB = new System.Windows.Forms.Button();
            this.Remove_PB = new System.Windows.Forms.Button();
            this.Add_PB = new System.Windows.Forms.Button();
            this.Unit_CB = new System.Windows.Forms.ComboBox();
            this.Quantity_Numeric = new System.Windows.Forms.NumericUpDown();
            this.Name_TB = new System.Windows.Forms.TextBox();
            this.Save_PB = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Ingredients_Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Quantity_Numeric)).BeginInit();
            this.SuspendLayout();
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.Location = new System.Drawing.Point(133, 9);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(268, 42);
            this.Title.TabIndex = 12;
            this.Title.Text = "Modify Recipe";
            // 
            // Note_Label
            // 
            this.Note_Label.AutoSize = true;
            this.Note_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Note_Label.Location = new System.Drawing.Point(35, 69);
            this.Note_Label.Name = "Note_Label";
            this.Note_Label.Size = new System.Drawing.Size(489, 18);
            this.Note_Label.TabIndex = 13;
            this.Note_Label.Text = "Note: Please fill in as much information as possible for the desired recipe.";
            // 
            // Recipe_Title_TB
            // 
            this.Recipe_Title_TB.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Recipe_Title_TB.Location = new System.Drawing.Point(99, 106);
            this.Recipe_Title_TB.Name = "Recipe_Title_TB";
            this.Recipe_Title_TB.Size = new System.Drawing.Size(463, 29);
            this.Recipe_Title_TB.TabIndex = 14;
            // 
            // Recipe_Description_TB
            // 
            this.Recipe_Description_TB.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Recipe_Description_TB.Location = new System.Drawing.Point(99, 161);
            this.Recipe_Description_TB.Multiline = true;
            this.Recipe_Description_TB.Name = "Recipe_Description_TB";
            this.Recipe_Description_TB.Size = new System.Drawing.Size(463, 56);
            this.Recipe_Description_TB.TabIndex = 15;
            // 
            // Recipe_Directions_TB
            // 
            this.Recipe_Directions_TB.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Recipe_Directions_TB.Location = new System.Drawing.Point(99, 237);
            this.Recipe_Directions_TB.Multiline = true;
            this.Recipe_Directions_TB.Name = "Recipe_Directions_TB";
            this.Recipe_Directions_TB.Size = new System.Drawing.Size(463, 56);
            this.Recipe_Directions_TB.TabIndex = 16;
            // 
            // Ingredients_Table
            // 
            this.Ingredients_Table.AllowUserToDeleteRows = false;
            this.Ingredients_Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Ingredients_Table.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Ingredient,
            this.Quantity,
            this.Unit});
            this.Ingredients_Table.Location = new System.Drawing.Point(99, 315);
            this.Ingredients_Table.MultiSelect = false;
            this.Ingredients_Table.Name = "Ingredients_Table";
            this.Ingredients_Table.ReadOnly = true;
            this.Ingredients_Table.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Ingredients_Table.Size = new System.Drawing.Size(346, 96);
            this.Ingredients_Table.TabIndex = 17;
            // 
            // Ingredient
            // 
            this.Ingredient.HeaderText = "Ingredient";
            this.Ingredient.Name = "Ingredient";
            this.Ingredient.ReadOnly = true;
            // 
            // Quantity
            // 
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.Name = "Quantity";
            this.Quantity.ReadOnly = true;
            // 
            // Unit
            // 
            this.Unit.HeaderText = "Unit";
            this.Unit.Name = "Unit";
            this.Unit.ReadOnly = true;
            // 
            // Title_Label
            // 
            this.Title_Label.AutoSize = true;
            this.Title_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title_Label.Location = new System.Drawing.Point(54, 113);
            this.Title_Label.Name = "Title_Label";
            this.Title_Label.Size = new System.Drawing.Size(39, 18);
            this.Title_Label.TabIndex = 18;
            this.Title_Label.Text = "Title:";
            // 
            // Description_Label
            // 
            this.Description_Label.AutoSize = true;
            this.Description_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Description_Label.Location = new System.Drawing.Point(12, 168);
            this.Description_Label.Name = "Description_Label";
            this.Description_Label.Size = new System.Drawing.Size(87, 18);
            this.Description_Label.TabIndex = 19;
            this.Description_Label.Text = "Description:";
            // 
            // Directions_Label
            // 
            this.Directions_Label.AutoSize = true;
            this.Directions_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Directions_Label.Location = new System.Drawing.Point(14, 244);
            this.Directions_Label.Name = "Directions_Label";
            this.Directions_Label.Size = new System.Drawing.Size(79, 18);
            this.Directions_Label.TabIndex = 20;
            this.Directions_Label.Text = "Directions:";
            // 
            // Ingredients_Label
            // 
            this.Ingredients_Label.AutoSize = true;
            this.Ingredients_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ingredients_Label.Location = new System.Drawing.Point(10, 315);
            this.Ingredients_Label.Name = "Ingredients_Label";
            this.Ingredients_Label.Size = new System.Drawing.Size(83, 18);
            this.Ingredients_Label.TabIndex = 21;
            this.Ingredients_Label.Text = "Ingredients:";
            // 
            // Unit_Label
            // 
            this.Unit_Label.AutoSize = true;
            this.Unit_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Unit_Label.Location = new System.Drawing.Point(480, 433);
            this.Unit_Label.Name = "Unit_Label";
            this.Unit_Label.Size = new System.Drawing.Size(34, 18);
            this.Unit_Label.TabIndex = 30;
            this.Unit_Label.Text = "Unit";
            // 
            // Quanitity_Label
            // 
            this.Quanitity_Label.AutoSize = true;
            this.Quanitity_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Quanitity_Label.Location = new System.Drawing.Point(343, 433);
            this.Quanitity_Label.Name = "Quanitity_Label";
            this.Quanitity_Label.Size = new System.Drawing.Size(65, 18);
            this.Quanitity_Label.TabIndex = 29;
            this.Quanitity_Label.Text = "Quantitiy";
            // 
            // Name_Label
            // 
            this.Name_Label.AutoSize = true;
            this.Name_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name_Label.Location = new System.Drawing.Point(96, 430);
            this.Name_Label.Name = "Name_Label";
            this.Name_Label.Size = new System.Drawing.Size(48, 18);
            this.Name_Label.TabIndex = 28;
            this.Name_Label.Text = "Name";
            // 
            // Cancel_PB
            // 
            this.Cancel_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancel_PB.Location = new System.Drawing.Point(471, 557);
            this.Cancel_PB.Name = "Cancel_PB";
            this.Cancel_PB.Size = new System.Drawing.Size(85, 35);
            this.Cancel_PB.TabIndex = 27;
            this.Cancel_PB.Text = "Cancel";
            this.Cancel_PB.UseVisualStyleBackColor = true;
            this.Cancel_PB.Click += new System.EventHandler(this.Cancel_PB_Click);
            // 
            // Remove_PB
            // 
            this.Remove_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Remove_PB.Location = new System.Drawing.Point(327, 487);
            this.Remove_PB.Name = "Remove_PB";
            this.Remove_PB.Size = new System.Drawing.Size(85, 35);
            this.Remove_PB.TabIndex = 26;
            this.Remove_PB.Text = "Remove";
            this.Remove_PB.UseVisualStyleBackColor = true;
            // 
            // Add_PB
            // 
            this.Add_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Add_PB.Location = new System.Drawing.Point(195, 487);
            this.Add_PB.Name = "Add_PB";
            this.Add_PB.Size = new System.Drawing.Size(85, 35);
            this.Add_PB.TabIndex = 25;
            this.Add_PB.Text = "Add";
            this.Add_PB.UseVisualStyleBackColor = true;
            // 
            // Unit_CB
            // 
            this.Unit_CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Unit_CB.FormattingEnabled = true;
            this.Unit_CB.Location = new System.Drawing.Point(432, 452);
            this.Unit_CB.Name = "Unit_CB";
            this.Unit_CB.Size = new System.Drawing.Size(124, 26);
            this.Unit_CB.TabIndex = 24;
            // 
            // Quantity_Numeric
            // 
            this.Quantity_Numeric.AllowDrop = true;
            this.Quantity_Numeric.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Quantity_Numeric.Location = new System.Drawing.Point(346, 452);
            this.Quantity_Numeric.Name = "Quantity_Numeric";
            this.Quantity_Numeric.Size = new System.Drawing.Size(55, 24);
            this.Quantity_Numeric.TabIndex = 23;
            // 
            // Name_TB
            // 
            this.Name_TB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name_TB.Location = new System.Drawing.Point(99, 451);
            this.Name_TB.Name = "Name_TB";
            this.Name_TB.Size = new System.Drawing.Size(208, 24);
            this.Name_TB.TabIndex = 22;
            // 
            // Save_PB
            // 
            this.Save_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Save_PB.Location = new System.Drawing.Point(327, 557);
            this.Save_PB.Name = "Save_PB";
            this.Save_PB.Size = new System.Drawing.Size(85, 35);
            this.Save_PB.TabIndex = 31;
            this.Save_PB.Text = "Save";
            this.Save_PB.UseVisualStyleBackColor = true;
            this.Save_PB.Click += new System.EventHandler(this.Save_Click);
            // 
            // Modify_Recipe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 604);
            this.Controls.Add(this.Save_PB);
            this.Controls.Add(this.Unit_Label);
            this.Controls.Add(this.Quanitity_Label);
            this.Controls.Add(this.Name_Label);
            this.Controls.Add(this.Cancel_PB);
            this.Controls.Add(this.Remove_PB);
            this.Controls.Add(this.Add_PB);
            this.Controls.Add(this.Unit_CB);
            this.Controls.Add(this.Quantity_Numeric);
            this.Controls.Add(this.Name_TB);
            this.Controls.Add(this.Ingredients_Label);
            this.Controls.Add(this.Directions_Label);
            this.Controls.Add(this.Description_Label);
            this.Controls.Add(this.Title_Label);
            this.Controls.Add(this.Ingredients_Table);
            this.Controls.Add(this.Recipe_Directions_TB);
            this.Controls.Add(this.Recipe_Description_TB);
            this.Controls.Add(this.Recipe_Title_TB);
            this.Controls.Add(this.Note_Label);
            this.Controls.Add(this.Title);
            this.Name = "Modify_Recipe";
            this.Text = "Modify_Recipe";
            ((System.ComponentModel.ISupportInitialize)(this.Ingredients_Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Quantity_Numeric)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label Note_Label;
        private System.Windows.Forms.TextBox Recipe_Title_TB;
        private System.Windows.Forms.TextBox Recipe_Description_TB;
        private System.Windows.Forms.TextBox Recipe_Directions_TB;
        private System.Windows.Forms.DataGridView Ingredients_Table;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ingredient;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unit;
        private System.Windows.Forms.Label Title_Label;
        private System.Windows.Forms.Label Description_Label;
        private System.Windows.Forms.Label Directions_Label;
        private System.Windows.Forms.Label Ingredients_Label;
        private System.Windows.Forms.Label Unit_Label;
        private System.Windows.Forms.Label Quanitity_Label;
        private System.Windows.Forms.Label Name_Label;
        private System.Windows.Forms.Button Cancel_PB;
        private System.Windows.Forms.Button Remove_PB;
        private System.Windows.Forms.Button Add_PB;
        private System.Windows.Forms.ComboBox Unit_CB;
        private System.Windows.Forms.NumericUpDown Quantity_Numeric;
        private System.Windows.Forms.TextBox Name_TB;
        private System.Windows.Forms.Button Save_PB;
    }
}