﻿namespace Recipe_Decisionator
{
    partial class Search_Recipe_Database
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Search_PB = new System.Windows.Forms.Button();
            this.Back_PB = new System.Windows.Forms.Button();
            this.Title = new System.Windows.Forms.Label();
            this.Note_Label = new System.Windows.Forms.Label();
            this.Recipe_Title_TB = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Search_PB
            // 
            this.Search_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Search_PB.Location = new System.Drawing.Point(263, 262);
            this.Search_PB.Name = "Search_PB";
            this.Search_PB.Size = new System.Drawing.Size(85, 35);
            this.Search_PB.TabIndex = 24;
            this.Search_PB.Text = "Search";
            this.Search_PB.UseVisualStyleBackColor = true;
            this.Search_PB.Click += new System.EventHandler(this.Search_PB_Click);
            // 
            // Back_PB
            // 
            this.Back_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Back_PB.Location = new System.Drawing.Point(512, 343);
            this.Back_PB.Name = "Back_PB";
            this.Back_PB.Size = new System.Drawing.Size(85, 35);
            this.Back_PB.TabIndex = 23;
            this.Back_PB.Text = "BACK";
            this.Back_PB.UseVisualStyleBackColor = true;
            this.Back_PB.Click += new System.EventHandler(this.Back_PB_Click);
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.Location = new System.Drawing.Point(81, 9);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(457, 42);
            this.Title.TabIndex = 22;
            this.Title.Text = "Search Recipe Database";
            // 
            // Note_Label
            // 
            this.Note_Label.AutoSize = true;
            this.Note_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Note_Label.Location = new System.Drawing.Point(100, 119);
            this.Note_Label.Name = "Note_Label";
            this.Note_Label.Size = new System.Drawing.Size(410, 18);
            this.Note_Label.TabIndex = 21;
            this.Note_Label.Text = "Note: Please enter the title of the recipe you would like to find.";
            // 
            // Recipe_Title_TB
            // 
            this.Recipe_Title_TB.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Recipe_Title_TB.Location = new System.Drawing.Point(102, 187);
            this.Recipe_Title_TB.Name = "Recipe_Title_TB";
            this.Recipe_Title_TB.Size = new System.Drawing.Size(407, 29);
            this.Recipe_Title_TB.TabIndex = 20;
            // 
            // Search_Recipe_Database
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 390);
            this.Controls.Add(this.Search_PB);
            this.Controls.Add(this.Back_PB);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.Note_Label);
            this.Controls.Add(this.Recipe_Title_TB);
            this.Name = "Search_Recipe_Database";
            this.Text = "Search_Recipe_Database";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Search_PB;
        private System.Windows.Forms.Button Back_PB;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label Note_Label;
        private System.Windows.Forms.TextBox Recipe_Title_TB;
    }
}