﻿namespace Recipe_Decisionator
{
    partial class Edit_Ingredients_Possessed
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Title = new System.Windows.Forms.Label();
            this.List_Ingredients_Label = new System.Windows.Forms.Label();
            this.Name_TB = new System.Windows.Forms.TextBox();
            this.Quantity = new System.Windows.Forms.NumericUpDown();
            this.Unit = new System.Windows.Forms.ComboBox();
            this.Add_PB = new System.Windows.Forms.Button();
            this.Remove_PB = new System.Windows.Forms.Button();
            this.Cancel_PB = new System.Windows.Forms.Button();
            this.Name_Label = new System.Windows.Forms.Label();
            this.Quanitity_Label = new System.Windows.Forms.Label();
            this.Unit_Label = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ingredients = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Save_PB = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Quantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.Location = new System.Drawing.Point(29, 9);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(497, 42);
            this.Title.TabIndex = 1;
            this.Title.Text = "Edit Ingredients Possessed";
            // 
            // List_Ingredients_Label
            // 
            this.List_Ingredients_Label.AutoSize = true;
            this.List_Ingredients_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.List_Ingredients_Label.Location = new System.Drawing.Point(102, 75);
            this.List_Ingredients_Label.Name = "List_Ingredients_Label";
            this.List_Ingredients_Label.Size = new System.Drawing.Size(145, 18);
            this.List_Ingredients_Label.TabIndex = 2;
            this.List_Ingredients_Label.Text = "List of all Ingredients.";
            // 
            // Name_TB
            // 
            this.Name_TB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name_TB.Location = new System.Drawing.Point(87, 66);
            this.Name_TB.Name = "Name_TB";
            this.Name_TB.Size = new System.Drawing.Size(208, 24);
            this.Name_TB.TabIndex = 3;
            // 
            // Quantity
            // 
            this.Quantity.AllowDrop = true;
            this.Quantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Quantity.Location = new System.Drawing.Point(297, 338);
            this.Quantity.Name = "Quantity";
            this.Quantity.Size = new System.Drawing.Size(55, 24);
            this.Quantity.TabIndex = 4;
            // 
            // Unit
            // 
            this.Unit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Unit.FormattingEnabled = true;
            this.Unit.Location = new System.Drawing.Point(402, 338);
            this.Unit.Name = "Unit";
            this.Unit.Size = new System.Drawing.Size(124, 26);
            this.Unit.TabIndex = 5;
            // 
            // Add_PB
            // 
            this.Add_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Add_PB.Location = new System.Drawing.Point(165, 373);
            this.Add_PB.Name = "Add_PB";
            this.Add_PB.Size = new System.Drawing.Size(85, 35);
            this.Add_PB.TabIndex = 6;
            this.Add_PB.Text = "Add";
            this.Add_PB.UseVisualStyleBackColor = true;
            this.Add_PB.Click += new System.EventHandler(this.Add_PB_Click);
            // 
            // Remove_PB
            // 
            this.Remove_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Remove_PB.Location = new System.Drawing.Point(297, 373);
            this.Remove_PB.Name = "Remove_PB";
            this.Remove_PB.Size = new System.Drawing.Size(85, 35);
            this.Remove_PB.TabIndex = 7;
            this.Remove_PB.Text = "Remove";
            this.Remove_PB.UseVisualStyleBackColor = true;
            this.Remove_PB.Click += new System.EventHandler(this.Remove_PB_Click);
            // 
            // Cancel_PB
            // 
            this.Cancel_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancel_PB.Location = new System.Drawing.Point(453, 434);
            this.Cancel_PB.Name = "Cancel_PB";
            this.Cancel_PB.Size = new System.Drawing.Size(85, 35);
            this.Cancel_PB.TabIndex = 8;
            this.Cancel_PB.Text = "Cancel";
            this.Cancel_PB.UseVisualStyleBackColor = true;
            this.Cancel_PB.Click += new System.EventHandler(this.Cancel_PB_Click);
            // 
            // Name_Label
            // 
            this.Name_Label.AutoSize = true;
            this.Name_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name_Label.Location = new System.Drawing.Point(24, 319);
            this.Name_Label.Name = "Name_Label";
            this.Name_Label.Size = new System.Drawing.Size(48, 18);
            this.Name_Label.TabIndex = 9;
            this.Name_Label.Text = "Name";
            // 
            // Quanitity_Label
            // 
            this.Quanitity_Label.AutoSize = true;
            this.Quanitity_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Quanitity_Label.Location = new System.Drawing.Point(291, 319);
            this.Quanitity_Label.Name = "Quanitity_Label";
            this.Quanitity_Label.Size = new System.Drawing.Size(65, 18);
            this.Quanitity_Label.TabIndex = 10;
            this.Quanitity_Label.Text = "Quantitiy";
            // 
            // Unit_Label
            // 
            this.Unit_Label.AutoSize = true;
            this.Unit_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Unit_Label.Location = new System.Drawing.Point(450, 319);
            this.Unit_Label.Name = "Unit_Label";
            this.Unit_Label.Size = new System.Drawing.Size(34, 18);
            this.Unit_Label.TabIndex = 11;
            this.Unit_Label.Text = "Unit";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.Ingredients,
            this.dataGridViewTextBoxColumn2});
            this.dataGridView1.Location = new System.Drawing.Point(105, 96);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(347, 208);
            this.dataGridView1.TabIndex = 21;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // Ingredients
            // 
            this.Ingredients.HeaderText = "Ingredients";
            this.Ingredients.Name = "Ingredients";
            this.Ingredients.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Unit";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // Save_PB
            // 
            this.Save_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Save_PB.Location = new System.Drawing.Point(330, 434);
            this.Save_PB.Name = "Save_PB";
            this.Save_PB.Size = new System.Drawing.Size(85, 35);
            this.Save_PB.TabIndex = 22;
            this.Save_PB.Text = "Save";
            this.Save_PB.UseVisualStyleBackColor = true;
            this.Save_PB.Click += new System.EventHandler(this.Save_PB_Click);
            // 
            // Edit_Ingredients_Possessed
            // 
            this.ClientSize = new System.Drawing.Size(561, 481);
            this.Controls.Add(this.Save_PB);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.Unit_Label);
            this.Controls.Add(this.Quanitity_Label);
            this.Controls.Add(this.Name_Label);
            this.Controls.Add(this.Cancel_PB);
            this.Controls.Add(this.Remove_PB);
            this.Controls.Add(this.Add_PB);
            this.Controls.Add(this.Unit);
            this.Controls.Add(this.Quantity);
            this.Controls.Add(this.Name_TB);
            this.Controls.Add(this.List_Ingredients_Label);
            this.Controls.Add(this.Title);
            this.Name = "Edit_Ingredients_Possessed";
            ((System.ComponentModel.ISupportInitialize)(this.Quantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label List_Ingredients_Label;
        private System.Windows.Forms.TextBox Name_TB;
        private System.Windows.Forms.NumericUpDown Quantity;
        private System.Windows.Forms.ComboBox Unit;
        private System.Windows.Forms.Button Add_PB;
        private System.Windows.Forms.Button Remove_PB;
        private System.Windows.Forms.Button Cancel_PB;
        private System.Windows.Forms.Label Name_Label;
        private System.Windows.Forms.Label Quanitity_Label;
        private System.Windows.Forms.Label Unit_Label;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ingredients;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Button Save_PB;
    }
}