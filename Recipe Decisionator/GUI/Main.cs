﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recipe_Decisionator
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
            Title.Text = "Recipe Decisionator";
            this.CenterToScreen();
        }

        private void Search_Recipe_DB_PB_Click(object sender, EventArgs e)
        {
            this.Hide();
            var myForm = new Search_Recipe_Database();
            myForm.TopMost = true;
            myForm.ShowDialog();
            this.Show();
        }

        private void Modify_Recipe_DB_PB_Click(object sender, EventArgs e)
        {
            this.Hide();
            var myForm = new Modify_Recipe_Database ();
            myForm.TopMost = true;
            myForm.ShowDialog();
            this.Show();
        }

        private void Modify_Ingredients_Possessed_PB_Click(object sender, EventArgs e)
        {
            this.Hide();
            var myForm = new Edit_Ingredients_Possessed();
            myForm.TopMost = true;
            myForm.ShowDialog();
            this.Show();
        }

        private void Generate_Shopping_List_PB_Click(object sender, EventArgs e)
        {
            this.Hide();
            var myForm = new Show_Edit_ShoppingList();
            myForm.TopMost = true;
            myForm.ShowDialog();
            this.Show();
        }

        private void Find_Recipe_Based_Off_Ingredients_PB_Click(object sender, EventArgs e)
        {
            //searchrecipes by ingredients possessed
            this.Hide();
            var myForm = new GUI.Search_Recipe_Database_by_Ingredients();
            myForm.TopMost = true;
            myForm.ShowDialog();
            this.Show();
        }
    }
}
