﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recipe_Decisionator
{
    public partial class Recipe_Search_Results : Form
    {
        public Recipe_Search_Results(List<Recipe> Recipe_List)
        {
            InitializeComponent();
            this.CenterToScreen();

            //use for loop to cycle through all recipes to get strings and titles. Load titles in combobox.
            
            //Search function returns list of recipe objects

            //SearchByIngredients

            //cylce through and convert recipes to strings
            //Cycle through and get names of recipes


            //for examples sake
            Recipe_Results_TB.AppendText("Title: Chicken Stew");
            Recipe_Results_TB.AppendText(Environment.NewLine);
            Recipe_Results_TB.AppendText(Environment.NewLine);
            Recipe_Results_TB.AppendText("Description: This is where you would have text describing chicken stew. dddddddddddddddddddddddddddddddddddddd");
            Recipe_Results_TB.AppendText(Environment.NewLine);
            Recipe_Results_TB.AppendText(Environment.NewLine);
            Recipe_Results_TB.AppendText("Title: Pot Roast");
            Recipe_Results_TB.AppendText(Environment.NewLine);
            Recipe_Results_TB.AppendText(Environment.NewLine);
            Recipe_Results_TB.AppendText("Description: This is where you would have text describing chicken stew. dddddd dddddddddddddddddddddddddddddddd");
            Selected_Recipe_CB.Items.Add("Chicken Stew");
            Selected_Recipe_CB.Items.Add("Pot Roast");
            Selected_Recipe_CB.Items.Add("test2");
          /*  foreach (Recipe RC in RecipeList)
            {

            }*/
            //Selected_Recipe_CB.Items.
            Selected_Recipe_CB.SelectedIndex = 0;
        }
        private void Back_PB_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Select_PB_Click(object sender, EventArgs e)
        {
            Recipe selectedRecipe = new Recipe();
            //Selected_Recipe_CB.SelectedIndex Use this index in the recipe results list to find the selected recipe and pass to recipe display
            this.Hide();
            var myForm = new Recipe_Display(selectedRecipe);
            myForm.TopMost = true;
            myForm.ShowDialog();
            this.Show();
        }
    }
}
