﻿namespace Recipe_Decisionator
{
    partial class Error_Ingredients_Not_Added
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Back_PB = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Back_PB
            // 
            this.Back_PB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Back_PB.Location = new System.Drawing.Point(123, 99);
            this.Back_PB.Name = "Back_PB";
            this.Back_PB.Size = new System.Drawing.Size(85, 35);
            this.Back_PB.TabIndex = 13;
            this.Back_PB.Text = "OK";
            this.Back_PB.UseVisualStyleBackColor = true;
            this.Back_PB.Click += new System.EventHandler(this.Back_PB_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(35, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(260, 18);
            this.label2.TabIndex = 12;
            this.label2.Text = "ingredients to the current shopping list.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(308, 18);
            this.label1.TabIndex = 11;
            this.label1.Text = "There was an issue while adding this Recipe\'s";
            // 
            // Error_Ingredients_Not_Added
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 146);
            this.Controls.Add(this.Back_PB);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Error_Ingredients_Not_Added";
            this.Text = "Error_Ingredients_Not_Added";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Back_PB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}