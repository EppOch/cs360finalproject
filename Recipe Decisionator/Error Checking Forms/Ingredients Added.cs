﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recipe_Decisionator
{
    public partial class Ingredients_Added : Form
    {
        public Ingredients_Added()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        private void Back_PB_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
